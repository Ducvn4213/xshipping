package product.dg.xshipping.login;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class LoginModel implements LoginInterfaces.ProvidedModelOps{
    private LoginInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    LoginModel(LoginInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void doLogin(final String username, final String password) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_username", username));
        paramList.add(new Param("_password", password));

        mService.login(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                mService.setCurrentUser(data);
                mPresenter.saveUserCredentials(username, password);
                mPresenter.onLoginSuccess();
                doUpdateRegistrationID();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    void doUpdateRegistrationID() {
        List<Param> paramList = new ArrayList<>();
        mService.updateRegistrationID(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                //TODO
            }

            @Override
            public void onFail(String error) {
                //TODO
            }
        });
    }

    @Override
    public void doLoginFacebook(String id,final String email,final String name) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_faceid", id));

        mService.loginFacebook(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                data.setName(name);
                data.setEmail(email);
                mService.setCurrentFacebookUser(data);
                mPresenter.saveUserCredentials("", "");
                mPresenter.onLoginSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }
}
