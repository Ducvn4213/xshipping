package product.dg.xshipping.login;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

interface LoginInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void loginSuccessCallback();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doLogin(String username, String password);
        void doLoginFacebook(String id, String email, String name);
        void openMain();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onLoginSuccess();
        void saveUserCredentials(String username, String password);
    }

    interface ProvidedModelOps {
        void doLogin(String username, String password);
        void doLoginFacebook(String id, String email, String name);
    }
}
