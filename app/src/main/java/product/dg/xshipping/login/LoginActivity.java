package product.dg.xshipping.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import product.dg.xshipping.MainActivity;
import product.dg.xshipping.R;
import product.dg.xshipping.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity implements LoginInterfaces.RequiredViewOps {

    TextView mRegister;
    LoginButton mFaceLogin_hide;
    Button mFaceLogin;
    Button mLogin;
    EditText mUsername;
    EditText mPassword;
    CallbackManager mCallbackManager;
    ProgressDialog mProgressDialog;

    private LoginInterfaces.ProvidedPresenterOps mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        init();

        bindingControl();
        setupControlEvents();
    }

    private void init() {
        LoginPresenter presenter = new LoginPresenter(LoginActivity.this);
        LoginModel model = new LoginModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    private void bindingControl() {
        mRegister = (TextView) findViewById(R.id.tv_register);
        mFaceLogin_hide = (LoginButton) findViewById(R.id.btn_login_face_hide);
        mFaceLogin = (Button) findViewById(R.id.btn_login_face);
        mLogin = (Button) findViewById(R.id.btn_login);
        mUsername = (EditText) findViewById(R.id.edt_username);
        mPassword = (EditText) findViewById(R.id.edt_password);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setupControlEvents() {
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = mUsername.getText().toString();
                String password = mPassword.getText().toString();

                mPresenter.doLogin(username, password);
            }
        });

        mFaceLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFaceLogin_hide.performClick();
            }
        });

        mFaceLogin_hide.setReadPermissions("public_profile", "email");
        mFaceLogin_hide.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    String id = object.getString("id");
                                    String email = object.getString("email");
                                    String name = object.getString("name");

                                    mPresenter.doLoginFacebook(id, email, name);
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                    LoginActivity.this.showDialog(R.string.dialog_title_error, R.string.dialog_message_login_facebook_fail);
                                    return;
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                return;
            }

            @Override
            public void onError(FacebookException e) {
                LoginActivity.this.showDialog(R.string.dialog_title_error, R.string.dialog_message_login_facebook_fail);
                return;
            }
        });
    }

    @Override
    public AppCompatActivity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void loginSuccessCallback() {
        mPresenter.openMain();
    }
}
