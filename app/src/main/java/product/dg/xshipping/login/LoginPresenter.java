package product.dg.xshipping.login;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

import product.dg.xshipping.MainActivity;
import product.dg.xshipping.R;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.Utils;

public class LoginPresenter implements LoginInterfaces.ProvidedPresenterOps, LoginInterfaces.RequiredPresenterOps {
    private WeakReference<LoginInterfaces.RequiredViewOps> mView;
    private LoginInterfaces.ProvidedModelOps mModel;

    @Override
    public void setView(LoginInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void doLogin(String username, String password) {

        if (username.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_username);
            return;
        }

        if (!Utils.isValidEmail(username.trim())) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_email_invalid);
            return;
        }

        if (password.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_password);
            return;
        }

        if (password.trim().length() < 6) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_password_too_short);
            return;
        }

        mView.get().showWaiting();
        mModel.doLogin(username, password);
    }

    @Override
    public void doLoginFacebook(String id, String email, String name) {
        mModel.doLoginFacebook(id, email, name);
    }

    @Override
    public void openMain() {
        mView.get().getActivity().finish();
    }

    LoginPresenter(LoginInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(LoginInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void onLoginSuccess() {
        mView.get().hideWaiting();
        mView.get().loginSuccessCallback();
    }

    @Override
    public void saveUserCredentials(String username, String password) {
        FSService.getInstance().saveUserCredential(mView.get().getActivity(), username, password);
    }
}
