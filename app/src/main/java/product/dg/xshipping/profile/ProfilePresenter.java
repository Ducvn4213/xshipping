package product.dg.xshipping.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

import product.dg.xshipping.R;
import product.dg.xshipping.change_password.ChangePasswordActivity;
import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;
import product.dg.xshipping.verify.VerifyActivity;

public class ProfilePresenter implements ProfileInterfaces.ProvidedPresenterOps, ProfileInterfaces.RequiredPresenterOps {
    private WeakReference<ProfileInterfaces.RequiredViewOps> mView;
    private ProfileInterfaces.ProvidedModelOps mModel;

    ProfilePresenter(ProfileInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(ProfileInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(ProfileInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void doUpdate(String name, String address, String phone, String bank_name, String bank_number, String bank_branch) {
        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_name);
            return;
        }

        if (address.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_address);
            return;
        }

        mView.get().showWaiting();
        mModel.doUpdate(name, address, phone, bank_name, bank_number, bank_branch);
    }

    @Override
    public void openChangePassword() {
        Intent intent = new Intent(mView.get().getParenActivity(), ChangePasswordActivity.class);
        ActivityCompat.startActivity(mView.get().getParenActivity(), intent, null);
    }

    @Override
    public void openVerify() {
        Intent intent = new Intent(mView.get().getParenActivity(), VerifyActivity.class);
        ActivityCompat.startActivity(mView.get().getParenActivity(), intent, null);
    }

    @Override
    public void openLogin() {
        Intent intent = new Intent(mView.get().getParenActivity(), LoginActivity.class);
        ActivityCompat.startActivity(mView.get().getParenActivity(), intent, null);
    }

    @Override
    public void updateDisplayUserInfo() {
        mModel.displayUserInfo();
    }

    @Override
    public void openImagePicker() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ActivityCompat.startActivityForResult(mView.get().getParenActivity(), Intent.createChooser(intent, "Select Picture"), FS.PICK_IMAGE_REQUEST_CODE, null);
    }

    @Override
    public void doUpdateAvatar() {
        mView.get().showWaiting();
        mModel.doUpdateAvatar();
    }

    @Override
    public void setCurrentAvatarBitmap(Bitmap bitmap) {
        mModel.setCurrentAvatarBitmap(bitmap);
    }

    @Override
    public void updateAvatarSuccess() {
        mView.get().hideWaiting();
        mView.get().getParenActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().updateAvatarSuccess();
            }
        });
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getParenActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getParenActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void onUpdateSuccess() {
        mView.get().hideWaiting();
        mView.get().updateSuccessCallback();
    }

    @Override
    public void displayUserInfo(final String name, final String email, final String address, final String phone, final String avatar, final String bank_name, final String bank_number, final String bank_branch, final boolean isActive, final boolean isFacebookLogged) {
        mView.get().getParenActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().displayUserInfo(name, email, address, phone, avatar, bank_name, bank_number, bank_branch, isActive, isFacebookLogged);
            }
        });
    }

    @Override
    public void displayUserInfo_None() {
        mView.get().getParenActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().displayUserInfo_None();
            }
        });
    }
}
