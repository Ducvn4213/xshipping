package product.dg.xshipping.profile;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;

import product.dg.xshipping.model.User;

public class ProfileInterfaces {
    interface RequiredViewOps {
        Activity getParenActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void updateSuccessCallback();

        void displayUserInfo(String name, String email, String address, String phone, String avatar, String bank_name, String bank_number, String bank_branch, boolean isActive, boolean isFacebookLogged);
        void displayUserInfo_None();
        void updateAvatarSuccess();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doUpdate(String name, String address, String phone, String bank_name, String bank_number, String bank_branch);
        void openChangePassword();
        void openVerify();
        void openLogin();
        void updateDisplayUserInfo();
        void openImagePicker();
        void doUpdateAvatar();
        void setCurrentAvatarBitmap(Bitmap bitmap);

    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onUpdateSuccess();
        void displayUserInfo(String name, String email, String address, String phone, String avatar, String bank_name, String bank_number, String bank_branch, boolean isActive, boolean isFacebookLogged);
        void displayUserInfo_None();
        void updateAvatarSuccess();
    }

    interface ProvidedModelOps {
        void doUpdate(String name, String address, String phone, String bank_name, String bank_number, String bank_branch);

        void setFacebookUser(User user);
        void setUser(User user);
        User getFacebookUser();
        User getuser();

        Boolean isFacebookUser();

        void displayUserInfo();

        void setCurrentAvatarBitmap(Bitmap bitmap);
        void doUpdateAvatar();
    }
}
