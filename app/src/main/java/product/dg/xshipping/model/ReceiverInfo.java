package product.dg.xshipping.model;

public class ReceiverInfo {
    String mName;
    String mPhone;
    String mAddress;
    String mProvince;
    String mDistrict;
    String mWard;

    public String getName() {
        return mName;
    }
    public void setName(String name) {
        this.mName = name;
    }

    public String getPhone() {
        return mPhone;
    }
    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public String getAddress() {
        return mAddress;
    }
    public void setAddress(String address) {
        this.mAddress = address;
    }

    public String getProvince() {
        return mProvince;
    }
    public void setProvince(String province) {
        this.mProvince = province;
    }

    public String getDistrict() {
        return mDistrict;
    }
    public void setDistrict(String district) {
        this.mDistrict = district;
    }

    public String getWard() {
        return mWard;
    }
    public void setWard(String ward) {
        this.mWard = ward;
    }

    public ReceiverInfo(){
        mName = "";
        mPhone = "";
        mAddress = "";
        mProvince = "";
        mDistrict = "";
        mWard = "";
    }
}
