package product.dg.xshipping.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    String mID;
    @SerializedName("account")
    String mAccount;
    @SerializedName("address")
    String mAddress;
    @SerializedName("email")
    String mEmail;
    @SerializedName("name")
    String mName;
    @SerializedName("phone")
    String mPhone;
    @SerializedName("is_active")
    String mIsActive;
    @SerializedName("avatar")
    String mAvatar;
    @SerializedName("token")
    String mToken;
    @SerializedName("bank_name")
    String mBankName;
    @SerializedName("bank_number")
    String mBankNumber;
    @SerializedName("bank_branch")
    String mBankBranch;


    public String getAvatar() {
        return mAvatar;
    }

    public String getID() {
        return mID;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getAccountID() {
        return mAccount;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getToken() {
        return mToken;
    }

    public String getBankName() {
        return mBankName;
    }

    public String getBankNumber() {
        return mBankNumber;
    }

    public String getBankBranch() {
        return mBankBranch;
    }

    public void setBankName(String data) {
        mBankName = data;
    }

    public void setBankNumber(String data) {
        mBankNumber = data;
    }

    public void setmBankBranch(String data) {
        mBankBranch = data;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public void setIsActive(String isActive) {
        this.mIsActive = isActive;
    }

    public boolean isActive() {
        if (mIsActive.equalsIgnoreCase("1")) {
            return true;
        }

        return false;
    }
}
