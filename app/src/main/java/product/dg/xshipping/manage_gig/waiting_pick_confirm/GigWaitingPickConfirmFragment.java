package product.dg.xshipping.manage_gig.waiting_pick_confirm;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.GigConfirmAdapter;
import product.dg.xshipping.adapter.GigPickConfirmAdapter;
import product.dg.xshipping.manage_gig.processing.GigProcessingFragment;
import product.dg.xshipping.manage_gig.waiting_pick.GigWaitingPickFragment;

public class GigWaitingPickConfirmFragment extends Fragment implements GigWaitingPickConfirmInterfaces.RequiredViewOps {
    private GigWaitingPickConfirmInterfaces.ProvidedPresenterOps mPresenter;

    ListView mGigListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig_fragment, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    private void bindingControls(View view) {
        mGigListView = (ListView) view.findViewById(R.id.lv_gig_manage);
    }

    private void setupControlEvents() {
        mGigListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mPresenter.performConfirmDialog(i);
            }
        });
    }

    private void init() {
        GigWaitingPickConfirmPresenter presenter = new GigWaitingPickConfirmPresenter(this);
        GigWaitingPickConfirmModel model = new GigWaitingPickConfirmModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) GigWaitingPickConfirmFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void updateAdapter(GigPickConfirmAdapter adapter) {
        if (mGigListView != null) {
            mGigListView.setAdapter(adapter);
        }
    }

    @Override
    public void performConfirmDialog(final int position, String title) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(R.string.dialog_confirm_title))
                .setMessage("Xác nhận shipper đã nhận hàng của đơn hàng " + title + "")
                .setNegativeButton(R.string.dialog_confirm_title, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.confirm(position, true);
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.dialog_button_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.confirm(position, false);
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void performReload() {
        mPresenter.loadData();

        GigProcessingFragment gigProcessingFragment = GigProcessingFragment.getInstance();
        GigWaitingPickFragment gigWaitingPickFragment = GigWaitingPickFragment.getInstance();

        if (gigProcessingFragment != null) {
            gigProcessingFragment.onResume();
        }

        if (gigWaitingPickFragment != null) {
            gigWaitingPickFragment.onResume();
        }
    }
}
