package product.dg.xshipping.manage_gig;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import product.dg.xshipping.R;
import product.dg.xshipping.manage_gig.done.GigDoneFragment;
import product.dg.xshipping.manage_gig.error.GigErrorFragment;
import product.dg.xshipping.manage_gig.processing.GigProcessingFragment;
import product.dg.xshipping.manage_gig.waiting.GigWaitingFragment;
import product.dg.xshipping.manage_gig.waiting_book_confirm.GigWaitingBookConfirmFragment;
import product.dg.xshipping.manage_gig.waiting_cod.GigWaitingCodFragment;
import product.dg.xshipping.manage_gig.waiting_pick.GigWaitingPickFragment;
import product.dg.xshipping.manage_gig.waiting_pick_confirm.GigWaitingPickConfirmFragment;
import product.dg.xshipping.send_gig.gig_info.GigInfoFragment;
import product.dg.xshipping.send_gig.receiver_info.ReceiverInfoFragment;
import product.dg.xshipping.send_gig.sender_info.SenderInfoFragment;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.FS;

public class ManageGigFragment extends Fragment implements ViewPager.OnPageChangeListener {

    SmartTabLayout mTab;
    ViewPager mViewPager;

    FragmentPagerItemAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig, container, false);

        bindingUI(view);
        setupUIData();
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            for (int i = 0; i < mAdapter.getCount(); i++) {
                Fragment fragment = mAdapter.getPage(i);
                fragment.onResume();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        FSService service = FSService.getInstance();
        mViewPager.setCurrentItem(service.getSubIndex());
        service.setSubIndex(0);
    }

    private void bindingUI(View view) {
        mTab = (SmartTabLayout) view.findViewById(R.id.stl_tab);
        mViewPager = (ViewPager) view.findViewById(R.id.vp_container);
    }

    private void setupUIData() {
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getContext());

        Bundle waiting = new Bundle();
        waiting.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_WAITING);
        creator.add(R.string.manage_gigs_waiting, GigWaitingFragment.class, waiting);

        Bundle waiting_book_confirm = new Bundle();
        waiting_book_confirm.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_WAITING_BOOK_CONFIRM);
        creator.add(R.string.manage_gigs_waiting_book_confirm, GigWaitingBookConfirmFragment.class, waiting_book_confirm);

        Bundle waiting_pick = new Bundle();
        waiting_pick.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_WAITING_PICK);
        creator.add(R.string.manage_gigs_waiting_pick, GigWaitingPickFragment.class, waiting_pick);

        Bundle waiting_pick_confirm = new Bundle();
        waiting_pick_confirm.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_WAITING_PICK_CONFIRM);
        creator.add(R.string.manage_gigs_waiting_pick_confirm, GigWaitingPickConfirmFragment.class, waiting_pick_confirm);

        Bundle processing = new Bundle();
        processing.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_PROCESSING);
        creator.add(R.string.manage_gigs_processing, GigProcessingFragment.class, processing);

        Bundle error = new Bundle();
        error.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_ERROR);
        creator.add(R.string.manage_gigs_error, GigErrorFragment.class, error);

        Bundle waiting_cod = new Bundle();
        waiting_cod.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_WAITING_COD);
        creator.add(R.string.manage_gigs_waiting_cod, GigWaitingCodFragment.class, waiting_cod);

        Bundle done = new Bundle();
        done.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_DONE);
        creator.add(R.string.manage_gigs_done, GigDoneFragment.class, done);

        mAdapter = new FragmentPagerItemAdapter(getChildFragmentManager(),
                creator.create());

        mViewPager.setAdapter(mAdapter);
        mTab.setCustomTabView(R.layout.layout_smart_tab, R.id.tv_ticket_tab_item);
        mTab.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);
    }

    private void setupControlEvents() {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void goToTab(int index) {
        mViewPager.setCurrentItem(index);
    }
}
