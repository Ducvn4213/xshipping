package product.dg.xshipping.manage_gig.done;

import android.content.Intent;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.model.GigItem;
import product.dg.xshipping.manage_gig.gig_detail.GigDetailActivity;
import product.dg.xshipping.util.FS;

public class GigDonePresenter implements GigDoneInterfaces.ProvidedPresenterOps, GigDoneInterfaces.RequiredPresenterOps {
    private WeakReference<GigDoneInterfaces.RequiredViewOps> mView;
    private GigDoneInterfaces.ProvidedModelOps mModel;

    GigDonePresenter(GigDoneInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(GigDoneInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        mModel.downloadData();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void goToDetail(int position) {
        int id = mModel.getIdFromPosition(position);
        Intent intent = new Intent(mView.get().getParentActivity(), GigDetailActivity.class);
        intent.putExtra(FS.BUNDLE_GIG_ID, id);
        intent.putExtra(FS.BUNDLE_GIG_SENDER, FS.GIG_DONE);
        mView.get().getParentActivity().startActivity(intent);
    }

    @Override
    public void updateAdapter(GigAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void updateNewData(final List<GigItem> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.updateNewData(data);
            }
        });
    }

    @Override
    public GigAdapter createAdapterWithData(List<GigItem> data) {
        GigAdapter adapter = new GigAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }
}
