package product.dg.xshipping.manage_gig.gig_detail;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.manage_gig.edit_gig.EditGigActivity;
import product.dg.xshipping.tracking_shipper.TrackingShipperActivity;
import product.dg.xshipping.util.FS;

public class GigDetailPresenter implements GigDetailInterfaces.ProvidedPresenterOps, GigDetailInterfaces.RequiredPresenterOps {
    private WeakReference<GigDetailInterfaces.RequiredViewOps> mView;
    private GigDetailInterfaces.ProvidedModelOps mModel;

    GigDetailPresenter(GigDetailInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(GigDetailInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData(int id) {
        mModel.downloadData(id);
    }

    @Override
    public void deleteItem() {
        mView.get().showWaiting();
        mModel.deleteItem();
    }

    @Override
    public void cancelItem() {
        mView.get().showWaiting();
        mModel.cancelItem();
    }

    @Override
    public void updateItem() {
        mModel.updateItem();
    }

    @Override
    public void trackingItem() {
        String _sid = mModel.getData().getID();
        Intent intent = new Intent(mView.get().getActivity(), TrackingShipperActivity.class);
        intent.putExtra(FS.BUNDLE_GIG_ID, _sid);
        mView.get().getActivity().startActivity(intent);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void presentData(final GigDetailItem data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().presentData(data);
            }
        });
    }

    @Override
    public void finish() {
        mView.get().hideWaiting();
        mView.get().getActivity().finish();
    }

    @Override
    public void moveToEdit() {
        Intent intent = new Intent(mView.get().getActivity(), EditGigActivity.class);
        intent.putExtra(FS.BUNDLE_GIG_DATA, mModel.getData());
        mView.get().getActivity().startActivity(intent);
    }
}
