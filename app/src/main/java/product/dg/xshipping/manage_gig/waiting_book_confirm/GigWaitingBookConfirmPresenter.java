package product.dg.xshipping.manage_gig.waiting_book_confirm;

import android.content.Intent;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.GigConfirmAdapter;
import product.dg.xshipping.adapter.GigPickConfirmAdapter;
import product.dg.xshipping.adapter.model.GigConfirmItem;
import product.dg.xshipping.adapter.model.GigItem;
import product.dg.xshipping.manage_gig.gig_detail.GigDetailActivity;
import product.dg.xshipping.util.FS;

public class GigWaitingBookConfirmPresenter implements GigWaitingBookConfirmInterfaces.ProvidedPresenterOps, GigWaitingBookConfirmInterfaces.RequiredPresenterOps {
    private WeakReference<GigWaitingBookConfirmInterfaces.RequiredViewOps> mView;
    private GigWaitingBookConfirmInterfaces.ProvidedModelOps mModel;

    GigWaitingBookConfirmPresenter(GigWaitingBookConfirmInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(GigWaitingBookConfirmInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        mModel.downloadData();
    }

    @Override
    public void performConfirmDialog(int position) {
        String title = mModel.getDataFromPosition(position).getTitle();
        mView.get().performConfirmDialog(position, title);
    }

    @Override
    public void confirm(int pos, Boolean result) {
        mModel.confirm(pos, result);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void updateAdapter(GigConfirmAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void updateNewData(final List<GigConfirmItem> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.updateNewData(data);
            }
        });
    }

    @Override
    public GigConfirmAdapter createAdapterWithData(List<GigConfirmItem> data) {
        GigConfirmAdapter adapter = new GigConfirmAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }

    @Override
    public void requestReload() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().performReload();
            }
        });
    }
}
