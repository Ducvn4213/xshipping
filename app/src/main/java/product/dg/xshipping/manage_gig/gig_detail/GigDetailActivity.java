package product.dg.xshipping.manage_gig.gig_detail;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.util.FS;

public class GigDetailActivity extends AppCompatActivity implements GigDetailInterfaces.RequiredViewOps {
    private GigDetailInterfaces.ProvidedPresenterOps mPresenter;
    ProgressDialog mProgressDialog;
    String mSender;

    TextView mTitle;
    int mCurrentGigID;

    TextView mReceiverName;
    TextView mReceiverPhone;
    TextView mReceiverAddress;
    TextView mReceiverCity;
    TextView mReceiverDistrict;
    TextView mReceiverWard;

    TextView mGigName;
    TextView mGigWeight;
    TextView mGigPaymentMethod;
    TextView mGigSize;
    TextView mGigDate;
    TextView mGigCod;
    TextView mGigNote;

    TextView mSenderName;
    TextView mSenderPhone;
    TextView mSenderAddress;
    TextView mSenderBankName;
    TextView mSenderBankNumber;
    TextView mSenderBankBank;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gig_detail);

        Intent mIntent = getIntent();
        mCurrentGigID = mIntent.getIntExtra(FS.BUNDLE_GIG_ID , 0);
        mSender = mIntent.getStringExtra(FS.BUNDLE_GIG_SENDER);

        bindingControls();
        setupControlEvents();

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.loadData(mCurrentGigID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_gig_detail_menu, menu);

        MenuItem tracking = (MenuItem) menu.findItem(R.id.action_tracking);
        MenuItem update = (MenuItem) menu.findItem(R.id.action_update);
        MenuItem delete = (MenuItem) menu.findItem(R.id.action_delete);
        MenuItem cancel = (MenuItem) menu.findItem(R.id.action_cancel);
        MenuItem viewShipper = (MenuItem) menu.findItem(R.id.action_view_shipper);


        tracking.setVisible(false);
        update.setVisible(false);
        delete.setVisible(false);
        cancel.setVisible(false);
        viewShipper.setVisible(false);

        switch (mSender) {
            case FS.GIG_DONE:
                delete.setVisible(true);
                viewShipper.setVisible(true);
                break;
            case FS.GIG_ERROR:
                delete.setVisible(true);
                viewShipper.setVisible(true);
                break;
            case FS.GIG_PROCESSING:
                tracking.setVisible(true);
                cancel.setVisible(true);
                viewShipper.setVisible(true);
                break;
            case FS.GIG_WAITING:
                update.setVisible(true);
                delete.setVisible(true);
                viewShipper.setVisible(true);
                break;
            case FS.GIG_WAITING_COD:
                //delete.setVisible(true);
                viewShipper.setVisible(true);
                break;
            case FS.GIG_WAITING_PICK:
                delete.setVisible(true);
                viewShipper.setVisible(true);
                break;
            default:
                break;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_tracking:
                mPresenter.trackingItem();
                break;
            case R.id.action_update:
                mPresenter.updateItem();
                break;
            case R.id.action_delete:
                final AlertDialog dialog = new AlertDialog.Builder(GigDetailActivity.this)
                        .setTitle(getString(R.string.dialog_notice_title))
                        .setMessage(getString(R.string.dialog_message_gig_detail_delete))
                        .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mPresenter.deleteItem();
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                if (!dialog.isShowing()) {
                    dialog.show();
                }
                break;
            case R.id.action_cancel:
                final AlertDialog dialog_cancel = new AlertDialog.Builder(GigDetailActivity.this)
                        .setTitle(getString(R.string.dialog_notice_title))
                        .setMessage(getString(R.string.dialog_message_gig_detail_cancel))
                        .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mPresenter.cancelItem();
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                if (!dialog_cancel.isShowing()) {
                    dialog_cancel.show();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mReceiverName = (TextView) findViewById(R.id.tv_receiver_name);
        mReceiverPhone = (TextView) findViewById(R.id.tv_receiver_phone);
        mReceiverAddress = (TextView) findViewById(R.id.tv_receiver_address);
        mReceiverCity = (TextView) findViewById(R.id.tv_receiver_city);
        mReceiverDistrict = (TextView) findViewById(R.id.tv_receiver_district);
        mReceiverWard = (TextView) findViewById(R.id.tv_receiver_ward);

        mGigName = (TextView) findViewById(R.id.tv_gig_name);
        mGigWeight = (TextView) findViewById(R.id.tv_gig_weight);
        mGigPaymentMethod = (TextView) findViewById(R.id.tv_gig_payment_method);
        mGigSize = (TextView) findViewById(R.id.tv_gig_size);
        mGigDate = (TextView) findViewById(R.id.tv_gig_pickup_date);
        mGigCod = (TextView) findViewById(R.id.tv_gig_cod);
        mGigNote = (TextView) findViewById(R.id.tv_gig_note);

        mSenderName = (TextView) findViewById(R.id.tv_sender_name);
        mSenderPhone = (TextView) findViewById(R.id.tv_sender_phone);
        mSenderAddress = (TextView) findViewById(R.id.tv_sender_address);
        mSenderBankName = (TextView) findViewById(R.id.tv_sender_bank_account_name);
        mSenderBankNumber = (TextView) findViewById(R.id.tv_sender_bank_account_number);
        mSenderBankBank = (TextView) findViewById(R.id.tv_sender_bank_account_bank);
    }

    void setupControlEvents() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(null);
        }

        mTitle.setText(getString(R.string.gig_detail_title));
    }

    void init() {
        GigDetailPresenter presenter = new GigDetailPresenter(this);
        GigDetailModel model = new GigDetailModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getActivity() {
        return GigDetailActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(GigDetailActivity.this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void presentData(GigDetailItem data) {
        setTextFor(mReceiverName, data.getReceiverName());
        setTextFor(mReceiverPhone, data.getReceiverPhone());
        setTextFor(mReceiverAddress, data.getReceiverAddress());
        setTextFor(mReceiverCity, data.getReceiverCity());
        setTextFor(mReceiverDistrict, data.getReceiverDistrict());
        setTextFor(mReceiverWard, data.getReceiverWard());

        setTextFor(mGigName, data.getGigName());
        setTextFor(mGigWeight, data.getGigWeight());
        setTextFor(mGigPaymentMethod, data.getGigPaymentMethod());
        setTextFor(mGigSize, data.getGigSize());
        setTextFor(mGigDate, data.getGigDate());
        setTextFor(mGigCod, data.getGigCod());
        setTextFor(mGigNote, data.getGigNote());

        setTextFor(mSenderName, data.getSenderName());
        setTextFor(mSenderPhone, data.getSenderPhone());
        setTextFor(mSenderAddress, data.getSenderAddress());
        setTextFor(mSenderBankName, data.getSenderBankName());
        setTextFor(mSenderBankNumber, data.getSenderBankNumber());
        setTextFor(mSenderBankBank, data.getSenderBankBank());
    }

    void setTextFor(TextView textView, String data) {
        if (data.trim().equalsIgnoreCase("")) {
            textView.setVisibility(View.GONE);
        }
        else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(data);
        }
    }
}
