package product.dg.xshipping.manage_gig.waiting_cod;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.model.GigItem;

public class GigWaitingCodInterfaces {

    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigAdapter adapter);
    }

    interface ProvidedPresenterOps {
        void loadData();
        void goToDetail(int position);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigAdapter adapter);
        void updateNewData(List<GigItem> data);
        GigAdapter createAdapterWithData(List<GigItem> data);
    }

    interface ProvidedModelOps {
        void downloadData();
        int getIdFromPosition(int position);
        void updateNewData(List<GigItem> data);
    }
}
