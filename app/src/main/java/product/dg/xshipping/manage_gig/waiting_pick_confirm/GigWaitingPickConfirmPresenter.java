package product.dg.xshipping.manage_gig.waiting_pick_confirm;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipping.adapter.GigConfirmAdapter;
import product.dg.xshipping.adapter.GigPickConfirmAdapter;
import product.dg.xshipping.adapter.model.GigConfirmItem;

public class GigWaitingPickConfirmPresenter implements GigWaitingPickConfirmInterfaces.ProvidedPresenterOps, GigWaitingPickConfirmInterfaces.RequiredPresenterOps {
    private WeakReference<GigWaitingPickConfirmInterfaces.RequiredViewOps> mView;
    private GigWaitingPickConfirmInterfaces.ProvidedModelOps mModel;

    GigWaitingPickConfirmPresenter(GigWaitingPickConfirmInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(GigWaitingPickConfirmInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        mModel.downloadData();
    }

    @Override
    public void performConfirmDialog(int position) {
        String title = mModel.getDataFromPosition(position).getTitle();
        mView.get().performConfirmDialog(position, title);
    }

    @Override
    public void confirm(int pos, Boolean result) {
        mModel.confirm(pos, result);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void updateAdapter(GigPickConfirmAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void updateNewData(final List<GigConfirmItem> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.updateNewData(data);
            }
        });
    }

    @Override
    public GigPickConfirmAdapter createAdapterWithData(List<GigConfirmItem> data) {
        GigPickConfirmAdapter adapter = new GigPickConfirmAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }

    @Override
    public void requestReload() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().performReload();
            }
        });
    }
}
