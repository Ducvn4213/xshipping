package product.dg.xshipping.manage_gig.gig_detail;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.adapter.model.GigItem;

public class GigDetailInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void presentData(GigDetailItem data);
    }

    interface ProvidedPresenterOps {
        void loadData(int id);
        void deleteItem();
        void cancelItem();
        void updateItem();
        void trackingItem();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void presentData(GigDetailItem data);
        void finish();
        void moveToEdit();
    }

    interface ProvidedModelOps {
        void downloadData(int id);
        void deleteItem();
        void cancelItem();
        void updateItem();
        GigDetailItem getData();
    }
}
