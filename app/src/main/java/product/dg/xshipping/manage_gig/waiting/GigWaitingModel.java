package product.dg.xshipping.manage_gig.waiting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.model.GigItem;
import product.dg.xshipping.adapter.model.GigItemList;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class GigWaitingModel implements GigWaitingInterfaces.ProvidedModelOps {

    private GigWaitingInterfaces.RequiredPresenterOps mPresenter;
    private GigAdapter mAdapter;
    private FSService mService = FSService.getInstance();

    GigWaitingModel(GigWaitingInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;

        List<GigItem> data = new ArrayList<>();
        mAdapter = mPresenter.createAdapterWithData(data);
        mPresenter.updateAdapter(mAdapter);
    }

    @Override
    public void downloadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "1"));

        mService.getGigTicket(paramList, new FSService.Callback<GigItemList>() {
            @Override
            public void onSuccess(GigItemList data) {
                mPresenter.updateNewData(data.getData());
            }

            @Override
            public void onFail(String error) {
                List<GigItem> data = new ArrayList<>();
                mPresenter.updateNewData(data);
                //mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public int getIdFromPosition(int position) {
        return mAdapter.getItem(position).getId();
    }

    @Override
    public void updateNewData(List<GigItem> data) {
        mAdapter.clear();
        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();
    }
}
