package product.dg.xshipping.manage_gig.waiting_pick_confirm;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipping.adapter.GigConfirmAdapter;
import product.dg.xshipping.adapter.GigPickConfirmAdapter;
import product.dg.xshipping.adapter.model.GigConfirmItem;

public class GigWaitingPickConfirmInterfaces {

    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigPickConfirmAdapter adapter);

        void performConfirmDialog(int position, String title);

        void performReload();
    }

    interface ProvidedPresenterOps {
        void loadData();
        void performConfirmDialog(int position);
        void confirm(int pos, Boolean result);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigPickConfirmAdapter adapter);
        void updateNewData(List<GigConfirmItem> data);
        GigPickConfirmAdapter createAdapterWithData(List<GigConfirmItem> data);

        void requestReload();
    }

    interface ProvidedModelOps {
        void downloadData();
        int getIdFromPosition(int position);
        GigConfirmItem getDataFromPosition(int position);
        void updateNewData(List<GigConfirmItem> data);
        void confirm(int pos, Boolean result);
    }
}
