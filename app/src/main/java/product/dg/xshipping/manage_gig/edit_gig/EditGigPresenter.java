package product.dg.xshipping.manage_gig.edit_gig;

import android.widget.ArrayAdapter;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.model.GigInfo;
import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.model.SenderInfo;
import product.dg.xshipping.service.database.model.DistrictResponse;
import product.dg.xshipping.service.database.model.WardResponse;

public class EditGigPresenter implements EditGigInterfaces.ProvidedPresenterOps, EditGigInterfaces.RequiredPresenterOps {
    private WeakReference<EditGigInterfaces.RequiredViewOps> mView;
    private EditGigInterfaces.ProvidedModelOps mModel;

    EditGigPresenter(EditGigInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(EditGigInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        GigDetailItem data = mModel.getData();
        mView.get().presentData(data);
    }

    @Override
    public void loadGeoData() {
        mModel.loadGeoData();
    }

    @Override
    public void onSelectProvince(int id) {
        mModel.loadDistricts(id);
    }

    @Override
    public void onSelectDistrict(int id) {
        mModel.loadWards(id);
    }

    @Override
    public void endEndEdit() {
        mModel.endUpdate();
    }

    @Override
    public void updateGigTicket(String receiverName, String receiverPhone, String receiverAdrress,
                                String receiverProvince, String receiverDistrict, String receiverWard,
                                String senderAddress, String gigName, String gigWeight,
                                String gigPaymentMethod, String gigSize, String gigDate,
                                String gigCod, String gigNote, String gigBankName, String gigBankNumber, String gigBankBank) {
        if (receiverName.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_name_missing);
            return;
        }

        if (receiverPhone.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_phone_missing);
            return;
        }

        if (receiverPhone.startsWith("09")) {
            if (receiverPhone.length() != 10) {
                mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_phone_wrong);
                return;
            }
        }

        if (receiverPhone.startsWith("01")) {
            if (receiverPhone.length() != 11) {
                mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_phone_wrong);
                return;
            }
        }

        if (receiverAdrress.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_address_missing);
            return;
        }

        if (receiverAdrress.matches(".*\\d+.*") == false || receiverAdrress.matches(".*[a-zA-Z]+.*") == false) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_address_wrong);
            return;
        }

        if (gigName.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_name_missing);
            return;
        }

        if (gigWeight.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_weight_missing);
            return;
        }

        if (gigPaymentMethod.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_payment_method_missing);
            return;
        }

        if (gigSize.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_size_missing);
            return;
        }

        if (gigDate.trim().equalsIgnoreCase("ngày lấy hàng")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_date_missing);
            return;
        }

        if (senderAddress.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_address_missing);
            return;
        }

        ReceiverInfo receiverInfo = new ReceiverInfo();
        receiverInfo.setName(receiverName);
        receiverInfo.setPhone(receiverPhone);
        receiverInfo.setAddress(receiverAdrress);
        receiverInfo.setProvince(receiverProvince);
        receiverInfo.setDistrict(receiverDistrict);
        receiverInfo.setWard(receiverWard);

        SenderInfo senderInfo = new SenderInfo();
        senderInfo.setAddress(senderAddress);
        senderInfo.setBankName(gigBankName);
        senderInfo.setBankNumber(gigBankNumber);
        senderInfo.setBankBank(gigBankBank);

        GigInfo gigInfo = new GigInfo();
        gigInfo.setName(gigName);
        gigInfo.setWeight(gigWeight);
        gigInfo.setPaymentMethod(gigPaymentMethod);
        gigInfo.setSize(gigSize);
        gigInfo.setCod(gigCod);
        gigInfo.setDate(gigDate);
        gigInfo.setNote(gigNote);

        mModel.updateGigTicket(receiverInfo, gigInfo, senderInfo);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void handleOnDistrictCallback(final DistrictResponse data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.handleOnDistrictCallback(data);
            }
        });
    }

    @Override
    public void handleOnWardCallback(final WardResponse data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.handleOnWardCallback(data);
            }
        });
    }

    @Override
    public void notifyChanged(final ArrayAdapter<String> districtAdapter, final ArrayAdapter<String> wardAdapter) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                districtAdapter.notifyDataSetChanged();
                wardAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void finish() {
        mView.get().getActivity().finish();
    }

    @Override
    public ArrayAdapter<String> createAdapterWithData(List<String> data) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mView.get().getActivity(), R.layout.layout_spinner_item_left,
                R.id.tv_item_text, data);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_left);
        return adapter;
    }

    @Override
    public void geoDataCallback(final ArrayAdapter<String> provinceAdapter, final ArrayAdapter<String> districtAdapter, final  ArrayAdapter<String> wardAdapter,
                                final int province, final int district, final int ward) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                provinceAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
                districtAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
                wardAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);

                mView.get().geoDataCallback(provinceAdapter, districtAdapter, wardAdapter);
                mView.get().geoDataIndexCallback(province, district, ward);
            }
        });
    }
}

