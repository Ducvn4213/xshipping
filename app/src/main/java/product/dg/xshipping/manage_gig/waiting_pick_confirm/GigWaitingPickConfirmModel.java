package product.dg.xshipping.manage_gig.waiting_pick_confirm;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.adapter.GigConfirmAdapter;
import product.dg.xshipping.adapter.GigPickConfirmAdapter;
import product.dg.xshipping.adapter.model.GigConfirmItem;
import product.dg.xshipping.adapter.model.GigConfirmItemList;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class GigWaitingPickConfirmModel implements GigWaitingPickConfirmInterfaces.ProvidedModelOps {

    private GigWaitingPickConfirmInterfaces.RequiredPresenterOps mPresenter;
    private GigPickConfirmAdapter mAdapter;
    private FSService mService = FSService.getInstance();

    GigWaitingPickConfirmModel(GigWaitingPickConfirmInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;

        List<GigConfirmItem> data = new ArrayList<>();
        mAdapter = mPresenter.createAdapterWithData(data);
        mPresenter.updateAdapter(mAdapter);
    }

    @Override
    public void downloadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "14"));

        mService.getGigConfirmTicket(paramList, new FSService.Callback<GigConfirmItemList>() {
            @Override
            public void onSuccess(GigConfirmItemList data) {
                mPresenter.updateNewData(data.getData());
            }

            @Override
            public void onFail(String error) {
                List<GigConfirmItem> data = new ArrayList<>();
                mPresenter.updateNewData(data);
            }
        });
    }

    @Override
    public int getIdFromPosition(int position) {
        return mAdapter.getItem(position).getId();
    }

    @Override
    public GigConfirmItem getDataFromPosition(int position) {
        return mAdapter.getItem(position);
    }

    @Override
    public void updateNewData(List<GigConfirmItem> data) {
        mAdapter.clear();
        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void confirm(int pos, Boolean result) {
        int id = getIdFromPosition(pos);

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_ticket", "" + id));
        if (result) {
            paramList.add(new Param("_result", "yes"));
        }
        else {
            paramList.add(new Param("_result", "no"));
        }

        mService.confirmFromWaitingPick(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.requestReload();
            }

            @Override
            public void onFail(String error) {
                mPresenter.requestReload();
            }
        });
    }
}
