package product.dg.xshipping.manage_gig.edit_gig;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.model.GigInfo;
import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.model.SenderInfo;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.database.model.DistrictResponse;
import product.dg.xshipping.service.database.model.WardResponse;
import product.dg.xshipping.service.network.Param;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;

public class EditGigModel implements EditGigInterfaces.ProvidedModelOps {
    private EditGigInterfaces.RequiredPresenterOps mPresenter;
    FSService mService = FSService.getInstance();
    GigDetailItem mData;

    ArrayAdapter<String> mWardAdapter;
    ArrayAdapter<String> mDistrictAdapter;
    ArrayAdapter<String> mProvinceAdapter;

    DistrictResponse mDistrictResponse;
    WardResponse mWardResponse;

    EditGigModel(EditGigInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loadDistricts(int province_id) {
        if (province_id == 0) {
            mPresenter.handleOnDistrictCallback(new DistrictResponse());
            return;
        }

        province_id--;
        String province = mService.getGeoData().getListProvince().get(province_id).getID();
        List<Param> params = new ArrayList<>();
        params.add(new Param("_id", province));

        mService.getDistricts(params, new FSService.Callback<DistrictResponse>() {
            @Override
            public void onSuccess(DistrictResponse data) {
                mService.setDistrictResponse(data);
                mPresenter.handleOnDistrictCallback(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_district_fail);
            }
        });
    }

    @Override
    public void loadWards(int district_id) {
        if (district_id == 0) {
            mPresenter.handleOnWardCallback(new WardResponse());
            return;
        }

        district_id--;
        String district = mDistrictResponse.getData().get(district_id).getID();
        List<Param> params = new ArrayList<>();
        params.add(new Param("_id", district));

        mService.getWards(params, new FSService.Callback<WardResponse>() {
            @Override
            public void onSuccess(WardResponse data) {
                mService.setWardResponse(data);
                mPresenter.handleOnWardCallback(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_ward_fail);
            }
        });
    }

    @Override
    public void handleOnDistrictCallback(DistrictResponse data) {
        mDistrictAdapter.clear();
        mDistrictAdapter.addAll(data.getStringListDistrict());
        if (data.getStringListDistrict().size() > 0) {
            mWardAdapter.clear();
            mWardAdapter.addAll(new WardResponse().getStringListWard());
            mPresenter.notifyChanged(mDistrictAdapter, mWardAdapter);
        }
        else {
            mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_district_fail);
        }
    }

    @Override
    public void handleOnWardCallback(WardResponse data) {
        mWardAdapter.clear();
        mWardAdapter.addAll(data.getStringListWard());
        if (data.getStringListWard().size() > 0) {
            mPresenter.notifyChanged(mDistrictAdapter, mWardAdapter);
        }
        else {
            mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_ward_fail);
        }
    }

    @Override
    public GigDetailItem getData() {
        return this.mData;
    }

    @Override
    public void setData(GigDetailItem item) {
        this.mData = item;
    }

    @Override
    public void loadGeoData() {
        mProvinceAdapter = mPresenter.createAdapterWithData(mService.getGeoData().getStringListProvince());
        loadDistrcitListFromCity(mData.getReceiverCity());
    }

    @Override
    public void endUpdate() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", Integer.toString(Integer.parseInt(mData.getID()))));

        mService.endUpdateGigTicket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.finish();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void updateGigTicket(ReceiverInfo receiverInfo, GigInfo gigInfo, SenderInfo senderInfo) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_name", receiverInfo.getName()));
        paramList.add(new Param("_phone", receiverInfo.getPhone()));
        paramList.add(new Param("_address", receiverInfo.getAddress()));
        paramList.add(new Param("_city", receiverInfo.getProvince()));
        paramList.add(new Param("_district", receiverInfo.getDistrict()));
        paramList.add(new Param("_ward", receiverInfo.getWard()));
        paramList.add(new Param("_sender_address", senderInfo.getAddress()));
        paramList.add(new Param("_gig_name", gigInfo.getName()));
        paramList.add(new Param("_weight", gigInfo.getWeight()));
        paramList.add(new Param("_payment_method", gigInfo.getPaymentMethod()));
        paramList.add(new Param("_size", gigInfo.getSize()));
        paramList.add(new Param("_date", gigInfo.getDate()));
        paramList.add(new Param("_cod", gigInfo.getCod()));
        paramList.add(new Param("_note", gigInfo.getNote()));
        paramList.add(new Param("_bank_name", senderInfo.getBankName()));
        paramList.add(new Param("_bank_number", senderInfo.getBankNumber()));
        paramList.add(new Param("_bank_bank", senderInfo.getBankBank()));
        paramList.add(new Param("_bank_bank", senderInfo.getBankBank()));
        paramList.add(new Param("_gig_ticket_id", mData.getID()));

        mService.updateGigTicket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.finish();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    void loadDistrcitListFromCity(String city) {
        int province_id = Utils.getPositionByName(mService.getGeoData().getStringListProvince(), city);

        province_id--;
        String province = mService.getGeoData().getListProvince().get(province_id).getID();
        List<Param> params = new ArrayList<>();
        params.add(new Param("_id", province));

        mService.getDistricts(params, new FSService.Callback<DistrictResponse>() {
            @Override
            public void onSuccess(DistrictResponse data) {
                mDistrictResponse = data;
                mDistrictAdapter = mPresenter.createAdapterWithData(data.getStringListDistrict());
                loadWardListFromDistrict(mData.getReceiverDistrict());
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_district_fail);
            }
        });
    }

    void loadWardListFromDistrict(String district) {
        int district_id = Utils.getPositionByName(mDistrictResponse.getStringListDistrict(), district);

        district_id--;
        String districtID = mDistrictResponse.getData().get(district_id).getID();
        List<Param> params = new ArrayList<>();
        params.add(new Param("_id", districtID));

        mService.getWards(params, new FSService.Callback<WardResponse>() {
            @Override
            public void onSuccess(WardResponse data) {
                mWardResponse = data;
                mWardAdapter = mPresenter.createAdapterWithData(mWardResponse.getStringListWard());

                int province = Utils.getPositionByName(mService.getGeoData().getStringListProvince(), mData.getReceiverCity());
                int district = Utils.getPositionByName(mDistrictResponse.getStringListDistrict(), mData.getReceiverDistrict());
                int ward = Utils.getPositionByName(mWardResponse.getStringListWard(), mData.getReceiverWard());

                mPresenter.geoDataCallback(mProvinceAdapter, mDistrictAdapter, mWardAdapter, province, district, ward);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_ward_fail);
            }
        });
    }
}

