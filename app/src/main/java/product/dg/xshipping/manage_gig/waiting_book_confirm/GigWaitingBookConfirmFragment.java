package product.dg.xshipping.manage_gig.waiting_book_confirm;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.GigConfirmAdapter;
import product.dg.xshipping.adapter.GigPickConfirmAdapter;
import product.dg.xshipping.manage_gig.waiting.GigWaitingFragment;
import product.dg.xshipping.manage_gig.waiting_pick.GigWaitingPickFragment;

public class GigWaitingBookConfirmFragment extends Fragment implements GigWaitingBookConfirmInterfaces.RequiredViewOps {
    private GigWaitingBookConfirmInterfaces.ProvidedPresenterOps mPresenter;

    ListView mGigListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig_fragment, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    private void bindingControls(View view) {
        mGigListView = (ListView) view.findViewById(R.id.lv_gig_manage);
    }

    private void setupControlEvents() {
        mGigListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mPresenter.performConfirmDialog(i);
            }
        });
    }

    private void init() {
        GigWaitingBookConfirmPresenter presenter = new GigWaitingBookConfirmPresenter(this);
        GigWaitingBookConfirmModel model = new GigWaitingBookConfirmModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) GigWaitingBookConfirmFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void updateAdapter(GigConfirmAdapter adapter) {
        if (mGigListView != null) {
            mGigListView.setAdapter(adapter);
        }
    }

    @Override
    public void performConfirmDialog(final int position, String title) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(R.string.dialog_confirm_title))
                .setMessage("Bạn có đồng ý cho shipper giao đơn hàng " + title + " hay không?")
                .setNegativeButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.confirm(position, true);
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.dialog_button_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.confirm(position, false);
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void performReload() {
        mPresenter.loadData();
        GigWaitingFragment gigWaitingFragment = GigWaitingFragment.getInstance();
        GigWaitingPickFragment gigWaitingPickFragment = GigWaitingPickFragment.getInstance();

        if (gigWaitingFragment != null) {
            GigWaitingFragment.getInstance().onResume();
        }

        if (gigWaitingPickFragment != null) {
            GigWaitingPickFragment.getInstance().onResume();
        }
    }
}
