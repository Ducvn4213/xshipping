package product.dg.xshipping.manage_gig.gig_detail;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class GigDetailModel implements GigDetailInterfaces.ProvidedModelOps {
    private GigDetailInterfaces.RequiredPresenterOps mPresenter;
    FSService mService = FSService.getInstance();
    GigDetailItem mData;

    GigDetailModel(GigDetailInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void downloadData(int id) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", Integer.toString(id)));

        mService.getGigTicketDetail(paramList, new FSService.Callback<GigDetailItem>() {
            @Override
            public void onSuccess(GigDetailItem data) {
                mData = data;
                User currentUser = mService.isFacebookLogin() ? mService.getCurrentFacebookUser() : mService.getCurrentUser();
                data.setSenderName(currentUser.getName());
                data.setSenderPhone(currentUser.getPhone());
                mPresenter.presentData(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void deleteItem() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", Integer.toString(Integer.parseInt(mData.getID()))));

        mService.deleteGigTicket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.finish();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void cancelItem() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", Integer.toString(Integer.parseInt(mData.getID()))));

        mService.cancelGigTicket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.finish();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void updateItem() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", Integer.toString(Integer.parseInt(mData.getID()))));

        mService.updatingGigTicket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.moveToEdit();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public GigDetailItem getData() {
        return mData;
    }
}
