package product.dg.xshipping;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.TintAwareDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.manage_gig.ManageGigFragment;
import product.dg.xshipping.profile.ProfileFragment;
import product.dg.xshipping.register.RegisterActivity;
import product.dg.xshipping.send_gig.SendGigFragment;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.TrackerGPS;
import product.dg.xshipping.service.cloud_messaging.NotificationService;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SmartTabLayout.TabProvider, ViewPager.OnPageChangeListener, MainInterfaces.RequiredViewOps {

    //menu
    Button mLogin;
    Button mLogout;
    Button mSendGig;
    Button mManageGig;
    Button mProfile;

    FragmentPagerItemAdapter mPagerAdapter;
    ViewPager mViewPager;
    TextView mTitle;
    FloatingActionButton mAddGigs;

    DrawerLayout drawer;
    Menu mMenu;

    ProgressDialog mProgressDialog;

    FSService mService = FSService.getInstance();

    private MainInterfaces.ProvidedPresenterOps mPresenter;

    private static MainActivity instance;
    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.FS_SplashScreen);
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        init();

        autoLogin();

        bindingControl();
        setupUI();

        setupControlEvents();
        setupTab();
    }

    void checkFromNotification() {
        String noti_title = NotificationService.getInstance().getLastNotiTitle();
        if (noti_title != null) {
            Toast.makeText(MainActivity.this, noti_title, Toast.LENGTH_SHORT).show();
            if (noti_title.trim().contains("tài xế đặt")) {
                goToTab(1, 1);
            }
            else if (noti_title.trim().contains("đã được lấy")) {
                goToTab(1, 3);
            }
            else if (noti_title.trim().contains("không thành công")) {
                goToTab(1, 5);
            }
            else if (noti_title.trim().contains("giao thành công")) {
                goToTab(1, 7);
            }
        }

        NotificationService.getInstance().setLastNotiTitle(null);
    }

    private void init() {
        MainPresenter presenter = new MainPresenter(this);
        MainModel model = new MainModel(presenter);

        presenter.setModel(model);
        mPresenter = presenter;
        requestAppPermission();
    }

    private void requestAppPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (permissions.size() == 0) {
            return;
        }

        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(MainActivity.this,
                perArr,
                FS.PERMISSIONS_REQUEST);
    }

    private void autoLogin() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            try {
                                String id = object.getString("id");
                                String email = object.getString("email");
                                String name = object.getString("name");

                                mPresenter.doLoginFacebook(id, email, name);
                            }
                            catch (Exception ex) {
                                ex.printStackTrace();
                                MainActivity.this.showDialog(R.string.dialog_title_error, R.string.dialog_message_login_facebook_fail);
                                return;
                            }

                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday");
            request.setParameters(parameters);
            request.executeAsync();
            return;
        }

        String username = mService.getUsernameCredential(MainActivity.this);
        String password = mService.getPasswordCredential(MainActivity.this);

        if (!username.trim().equals("") && !password.trim().equals("")) {
            mPresenter.doLogin(username, password);
        }
    }

    private void bindingControl() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mAddGigs = (FloatingActionButton) findViewById(R.id.fab_add);
        mViewPager = (ViewPager) findViewById(R.id.vp_container);
        mLogin = (Button) findViewById(R.id.btn_login);
        mLogout = (Button) findViewById(R.id.btn_logout);
        mSendGig = (Button) findViewById(R.id.btn_send_gig);
        mManageGig = (Button) findViewById(R.id.btn_manage_gig);
        mProfile = (Button) findViewById(R.id.btn_profile);
    }

    private void setupUI() {
        mAddGigs.hide();

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(null);
        }

        drawer = (DrawerLayout) findViewById(R.id.dl_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void setupControlEvents() {
        mAddGigs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle(getString(R.string.dialog_notice_title))
                        .setMessage(getString(R.string.dialog_message_logout))
                        .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mService.logout();
                                mService.saveUserCredential(MainActivity.this, "", "");

                                if (mService.isLogin()) {
                                    mLogin.setVisibility(View.GONE);
                                    mLogout.setVisibility(View.VISIBLE);
                                }
                                else {
                                    mLogin.setVisibility(View.VISIBLE);
                                    mLogout.setVisibility(View.GONE);
                                }

                                dialogInterface.dismiss();
                                drawer.closeDrawer(GravityCompat.START, false);
                                try {
                                    ProfileFragment profileFragment = (ProfileFragment) mPagerAdapter.getPage(2);
                                    profileFragment.disableEdit();
                                    profileFragment.displayUserInfo_None();
                                    MenuItem update = (MenuItem) mMenu.findItem(R.id.action_update);
                                    update.setVisible(false);

                                    ManageGigFragment manageGigFragment  = (ManageGigFragment) mPagerAdapter.getPage(1);
                                    manageGigFragment.onResume();
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                drawer.closeDrawer(GravityCompat.START, false);
                            }
                        })
                        .create();
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }
        });

        mSendGig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
            }
        });

        mManageGig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawer.closeDrawer(GravityCompat.START, false);

        MenuItem update = null;
        if (mMenu != null) {
            update = mMenu.findItem(R.id.action_update);
        }
        if (mService.isLogin()) {
            mLogin.setVisibility(View.GONE);
            mLogout.setVisibility(View.VISIBLE);

            if (update != null) {
                update.setVisible(true);
            }
        }
        else {
            mLogin.setVisibility(View.VISIBLE);
            mLogout.setVisibility(View.GONE);
            if (update != null) {
                update.setVisible(false);
            }
        }

        if (update != null) {
            int currentViewPageIndex = mViewPager.getCurrentItem();
            if (currentViewPageIndex == 1) {
                update.setVisible(false);
            }
            else if (currentViewPageIndex == 0) {
                update.setVisible(false);
            }
            else {
                if (FSService.getInstance().isLogin()) {
                    update.setVisible(true);
                }
                else {
                    update.setVisible(false);
                }

            }
        }

        checkFromNotification();
        instance = MainActivity.this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        mMenu = menu;
        MenuItem update = (MenuItem) mMenu.findItem(R.id.action_update);

        int currentViewPageIndex = mViewPager.getCurrentItem();
        if (currentViewPageIndex == 1) {
            update.setVisible(false);
        }
        else if (currentViewPageIndex == 0) {
            update.setVisible(false);
        }
        else {
            if (FSService.getInstance().isLogin()) {
                update.setVisible(true);
            }
            else {
                update.setVisible(false);
            }

        }

        onResume();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_update:
                ProfileFragment fragment = (ProfileFragment) mPagerAdapter.getPage(2);
                if (fragment.isEdit()) {
                    fragment.doUpdateProfileInfo();
                    item.setTitle(getString(R.string.menu_edit));
                }
                else {
                    fragment.enableEdit();
                    item.setTitle(getString(R.string.menu_update));
                }
                break;
            default: break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        drawer = (DrawerLayout) findViewById(R.id.dl_drawer);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void setupTab() {
        mPagerAdapter = new FragmentPagerItemAdapter(getSupportFragmentManager(),
                FragmentPagerItems.with(this)
                        .add(R.string.main_title_send_gig, SendGigFragment.class)
                        .add(R.string.main_title_manage_gig, ManageGigFragment.class)
                        .add(R.string.main_title_profile, ProfileFragment.class)
                        .create());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mPagerAdapter);

        // Hook up the view pager with the
        //SmartTabLayout smartTabLayout = (SmartTabLayout) findViewById(R.id.stl_tab);
        //smartTabLayout.setCustomTabView(this);
        //smartTabLayout.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);

        changeTitle(mPagerAdapter.getPageTitle(0));
    }

    private void changeTitle(final CharSequence title) {
        mTitle.setText(title);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Utils.hideKeyboard(this);
        changeTitle(mPagerAdapter.getPageTitle(position));
        invalidateOptionsMenu();

        if (position != 1) {
            mAddGigs.hide();
        }
        else {
            mAddGigs.show();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View tab = inflater.inflate(R.layout.layout_tab_main, container, false);

        ImageView icon = (ImageView) tab.findViewById(R.id.iv_icon);
        switch (position) {
            case 0:
                icon.setImageResource(R.mipmap.tab_send_gig);
                break;
            case 1:
                icon.setImageResource(R.mipmap.tab_manage);
                break;
            case 2:
                icon.setImageResource(R.mipmap.tab_people);
                break;
            default:
                break;
        }

        TextView title = (TextView) tab.findViewById(R.id.tv_title);
        title.setText(adapter.getPageTitle(position));
        return tab;
    }

    @Override
    public AppCompatActivity getActivity() {
        return MainActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void loginSuccessCallback() {
        onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        android.support.v4.app.Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.vp_container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void goToTab(int index) {
        android.support.v4.app.Fragment fragment = mPagerAdapter.getPage(index);
        if (fragment != null) {
                fragment.onResume();
        }
        mViewPager.setCurrentItem(index);
    }

    public void goToTab(int index, int subindex) {
        android.support.v4.app.Fragment fragment = mPagerAdapter.getPage(index);
        if (fragment != null) {
            fragment.onResume();
        }
        mViewPager.setCurrentItem(index);
        mService.setSubIndex(subindex);
    }
    AlertDialog wrongTokenDialog;
    public void tokenFail() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mService.logout();
                mService.saveUserCredential(MainActivity.this, "", "");

                if (mService.isLogin()) {
                    mLogin.setVisibility(View.GONE);
                    mLogout.setVisibility(View.VISIBLE);
                }
                else {
                    mLogin.setVisibility(View.VISIBLE);
                    mLogout.setVisibility(View.GONE);
                }

                drawer.closeDrawer(GravityCompat.START, false);
                try {
                    ProfileFragment profileFragment = (ProfileFragment) mPagerAdapter.getPage(2);
                    profileFragment.disableEdit();
                    profileFragment.displayUserInfo_None();
                    MenuItem update = (MenuItem) mMenu.findItem(R.id.action_update);
                    update.setVisible(false);

                    ManageGigFragment manageGigFragment  = (ManageGigFragment) mPagerAdapter.getPage(1);
                    manageGigFragment.onResume();
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }

                if (wrongTokenDialog == null) {
                    wrongTokenDialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle(getString(R.string.dialog_notice_title))
                            .setMessage(getString(R.string.dialog_message_token_fail))
                            .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .create();
                }

                if (!wrongTokenDialog.isShowing()) {
                    wrongTokenDialog.show();
                }
            }
        });
    }
}
