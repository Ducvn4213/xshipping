package product.dg.xshipping;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class MainModel implements MainInterfaces.ProvidedModelOps {

    private MainInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    MainModel(MainInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void doLogin(final String username, final String password) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_username", username));
        paramList.add(new Param("_password", password));

        mService.login(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                mService.setCurrentUser(data);
                mPresenter.onLoginSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void doLoginFacebook(String id,final String email,final String name) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_faceid", id));

        mService.loginFacebook(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                data.setName(name);
                data.setEmail(email);
                mService.setCurrentFacebookUser(data);
                mPresenter.onLoginSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }
}
