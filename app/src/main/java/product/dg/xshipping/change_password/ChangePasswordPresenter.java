package product.dg.xshipping.change_password;

import java.lang.ref.WeakReference;

import product.dg.xshipping.R;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.verify.VerifyInterfaces;

public class ChangePasswordPresenter implements ChangePasswordInterfaces.ProvidedPresenterOps, ChangePasswordInterfaces.RequiredPresenterOps{
    private WeakReference<ChangePasswordInterfaces.RequiredViewOps> mView;
    private ChangePasswordInterfaces.ProvidedModelOps mModel;

    ChangePasswordPresenter(ChangePasswordInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(ChangePasswordInterfaces.ProvidedModelOps model) {
        mModel = model;
    }


    @Override
    public void setView(ChangePasswordInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void doChangePassword(String password_old, String password_new, String password_renew) {
        if (password_old.trim().equalsIgnoreCase("")) {
            showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_old_password);
            return;
        }

        if (password_old.trim().length() < 6) {
            showDialog(R.string.dialog_title_error, R.string.dialog_message_old_password_too_short);
            return;
        }

        if (password_new.trim().equalsIgnoreCase("")) {
            showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_new_password);
            return;
        }

        if (password_new.trim().length() < 6) {
            showDialog(R.string.dialog_title_error, R.string.dialog_message_new_password_too_short);
            return;
        }

        if (!password_new.trim().equals(password_renew.trim())) {
            showDialog(R.string.dialog_title_error, R.string.dialog_message_password_doesnt_match);
            return;
        }

        mModel.doChangePassword(password_old, password_new);
    }

    @Override
    public void openMain() {
        mView.get().getActivity().finish();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void onChangePasswordSuccess() {
        openMain();
    }

    @Override
    public void saveUserCredentials(String username, String password) {
        FSService.getInstance().saveUserCredential(mView.get().getActivity(), username, password);
    }
}
