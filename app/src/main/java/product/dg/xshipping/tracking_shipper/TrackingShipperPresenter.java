package product.dg.xshipping.tracking_shipper;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.Position;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.Utils;

public class TrackingShipperPresenter implements TrackingShipperInterfaces.ProvidedPresenterOps, TrackingShipperInterfaces.RequiredPresenterOps {
    private WeakReference<TrackingShipperInterfaces.RequiredViewOps> mView;
    private TrackingShipperInterfaces.ProvidedModelOps mModel;

    TrackingShipperPresenter(TrackingShipperInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(TrackingShipperInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void showDialog(int title, int message) {

    }

    @Override
    public void showDialog(int title, String message) {

    }

    @Override
    public void presentData(final Position data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().presentData(data);
            }
        });
    }

    @Override
    public void loadData() {
        mModel.loadData();
    }
}
