package product.dg.xshipping.tracking_shipper;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import product.dg.xshipping.R;
import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.model.Position;
import product.dg.xshipping.service.TrackerGPS;
import product.dg.xshipping.util.FS;

public class TrackingShipperActivity extends AppCompatActivity implements OnMapReadyCallback, TrackingShipperInterfaces.RequiredViewOps {

    private TrackingShipperInterfaces.ProvidedPresenterOps mPresenter;
    private GoogleMap mMap;
    private ProgressDialog mProgressDialog;
    SupportMapFragment mMapFragment;
    String mCurrentGigID;
    Timer mTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);

        bindingControls();
        setupControlEvents();

        init();
    }

    void bindingControls() {
        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.f_map);
    }

    void setupControlEvents() {}

    void init() {
        Intent mIntent = getIntent();
        mCurrentGigID = mIntent.getStringExtra(FS.BUNDLE_GIG_ID);

        mMapFragment.getMapAsync(this);

        TrackingShipperPresenter presenter = new TrackingShipperPresenter(this);
        TrackingShipperModel model = new TrackingShipperModel(presenter);
        model.setGigID(mCurrentGigID);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        LatLng defaultLocation = getCurrentLocationLatLng();
        mMap.addMarker(new MarkerOptions().position(defaultLocation).title(getString(R.string.get_current_location_your_location)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

        mPresenter.loadData();

        if (mTimer == null) {
            mTimer = new Timer();
        }

        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mPresenter.loadData();
            }
        }, 5000, 5000);
    }

    private LatLng getCurrentLocationLatLng() {
        LatLng defaultLocation = new LatLng(0, 0);
        TrackerGPS trackerGPS = new TrackerGPS(TrackingShipperActivity.this);
        if (trackerGPS.canGetLocation()) {
            if (trackerGPS.getLatitude() == 0 && trackerGPS.getLongitude() == 0) {
                return getCurrentLocationLatLng();
            }
            defaultLocation = new LatLng(trackerGPS.getLatitude(), trackerGPS.getLongitude());
            return defaultLocation;
        }

        return defaultLocation;
    }

    @Override
    public AppCompatActivity getActivity() {
        return TrackingShipperActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(TrackingShipperActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(TrackingShipperActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(TrackingShipperActivity.this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void presentData(Position data) {
        mMap.clear();
        LatLng latLng = new LatLng(data.getLatitude(), data.getLongitude());
        MarkerOptions options = new MarkerOptions();
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.shipper);

        options.flat(true);
        options.icon(icon);
        options.position(latLng);
        options.title("Shipper");

        mMap.addMarker(options);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(13));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
        mTimer = null;
    }
}
