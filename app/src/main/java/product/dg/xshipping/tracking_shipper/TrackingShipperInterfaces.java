package product.dg.xshipping.tracking_shipper;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipping.model.Position;

interface TrackingShipperInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void presentData(Position data);
    }

    interface ProvidedPresenterOps {
        void loadData();

    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void presentData(Position data);
    }

    interface ProvidedModelOps {
        void loadData();
        void setGigID(String gigID);
    }
}
