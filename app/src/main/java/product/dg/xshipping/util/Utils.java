package product.dg.xshipping.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Address;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Utils {
    public static void hideKeyboard(@NonNull Activity activity) {// Check if no view has focus:
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static void saveValue(Context context, String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public final static String getValue(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String value = preferences.getString(key, "");
        return value;
    }

    private static String bitMapToString(Bitmap bitmap){
        Bitmap scaleBitmap = bitmap.createScaledBitmap(bitmap, 1000, 1333, true);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        scaleBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte [] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public interface ConvertBitmapToStringCallback {
        void onSuccess(String bitmapString);
        void onFail();
    }

    public static class ConvertBitmapToStringTask extends AsyncTask<String, Void, String> {

        private Bitmap mBitmap;
        private ConvertBitmapToStringCallback mCallback;

        public void setBitmap(Bitmap bitmap) {
            this.mBitmap = bitmap;
        }

        public void setCallback(ConvertBitmapToStringCallback callback) {
            this.mCallback = callback;
        }

        protected String doInBackground(String... bitmap) {
            return bitMapToString(this.mBitmap);
        }

        protected void onPostExecute(String result) {
            if (mCallback != null) {
                mCallback.onSuccess(result);
            }
        }
    }

    public static String getStringAddressFromAddress(Address address) {
        String addressString = "";
        int maxAddressLineIndex = address.getMaxAddressLineIndex();
        for (int i = 0; i <= maxAddressLineIndex; i++) {
            addressString += address.getAddressLine(i) + ", ";
        }
        addressString = addressString.substring(0, addressString.length()-2);
        return addressString;
    }

    public static DateTime incrementDateByOne(DateTime date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date.toDate());
        c.add(Calendar.DATE, 1);
        Date nextDate = c.getTime();
        DateTime newDateTime = new DateTime();

        Long milliseconds = nextDate.getTime();
        Long ticks = milliseconds * 10000;
        newDateTime.withMillis(ticks);
        return newDateTime;
    }

    public static int getPositionByName(List<String> data, String name) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).equalsIgnoreCase(name)) {
                return i;
            }
        }

        return 0;
    }
}
