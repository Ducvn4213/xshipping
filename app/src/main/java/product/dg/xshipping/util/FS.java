package product.dg.xshipping.util;

public class FS {
    public static final String DATE_PICKER_START_DIALOG_TAG = "StartDatePicker";
    public static final int FACEBOOK_SMS_REQUEST_CODE = 99;
    public static final int PICK_IMAGE_REQUEST_CODE = 40;
    public static final int PICK_LOCATION_REQUEST_CODE = 41;
    public static final String WEB_API = "http://timshipper.net/api/api.php";
    public static final String WEB_API_GET_DATA = "http://timshipper.net/api/getgeodata.php";
    public static final String IMAGE_HOST = "http://timshipper.net/api/";

    public static final String BUNDLE_GIG_ID = "bundle_gig_id";
    public static final String BUNDLE_GIG_DATA = "bundle_gig_data";
    public static final String BUNDLE_GIG_SENDER = "bundle_gig_sender";

    public static final String SP_USERNAME_KEY = "username";
    public static final String SP_PASSWORD_KEY = "password";
    public static final String SP_FIST_TIME = "first_time";
    public static final String SP_FIST_TIME_VALUE = "first_time_value";

    public static final String EXTRA_SEND_GIG = "product.dg.xshipping.EXTRA_SEND_GIG";
    public static final String EXTRA_MANAGE_GIG = "product.dg.xshipping.EXTRA_MANAGE_GIG";

    public static final String RECEIVER_INFO = "receiver_info";
    public static final String GIG_INFO = "gig_info";
    public static final String SENDER_INFO = "sender_info";

    public static final String GIG_WAITING = "gig_waiting";
    public static final String GIG_WAITING_PICK = "gig_waiting_pick";
    public static final String GIG_WAITING_PICK_CONFIRM = "gig_waiting_pick_confirm";
    public static final String GIG_WAITING_BOOK_CONFIRM = "gig_waiting_book_confirm";
    public static final String GIG_PROCESSING = "gig_processing";
    public static final String GIG_ERROR = "gig_error";
    public static final String GIG_WAITING_COD = "gig_waiting_cod";
    public static final String GIG_DONE = "gig_done";

    public static final int PERMISSIONS_REQUEST = 80;

    public static final String NOTIFICATION_FLAG = "NOTIFICATION_FLAG";
}

