package product.dg.xshipping.service.cloud_messaging;

public class NotificationService {

    private static NotificationService instance;
    public static NotificationService getInstance() {
        if (instance == null) {
            instance = new NotificationService();
        }

        return instance;
    }

    private String mLastNotiTitle;

    private NotificationService() {}

    public void setLastNotiTitle(String data) {
        mLastNotiTitle = data;
    }

    public String getLastNotiTitle() {
        return mLastNotiTitle;
    }

}
