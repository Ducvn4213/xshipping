package product.dg.xshipping.service.database.model;

import com.google.gson.annotations.SerializedName;

public class Ward {
    public static final String TABLE_NAME = "ward";
    public static final String WARD_ID = "wardid";
    public static final String WARD_NAME = "name";
    public static final String WARD_TYPE = "type";
    public static final String WARD_LOCATION = "location";
    public static final String DISTRICT_ID = "districtid";

    public static final String CREATE_COMMAND =
            "CREATE TABLE IF NOT EXISTS " +TABLE_NAME + "(" +
                    WARD_ID + " TEXT NOT NULL PRIMARY KEY," +
                    WARD_NAME + " TEXT NOT NULL," +
                    WARD_TYPE + " TEXT NOT NULL," +
                    WARD_LOCATION + " TEXT NOT NULL," +
                    DISTRICT_ID + " TEXT NOT NULL)";

    public static final String DROP_COMMAND =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    @SerializedName("wardid")
    String mID;
    @SerializedName("name")
    String mName;
    @SerializedName("type")
    String mType;
    @SerializedName("location")
    String mLocation;
    @SerializedName("districtid")
    String mDistrictID;

    public String getID() {
        return mID;
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getDistrictID() {
        return mDistrictID;
    }


    public Ward() {}
}
