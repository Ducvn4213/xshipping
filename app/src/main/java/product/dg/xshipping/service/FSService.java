package product.dg.xshipping.service;

import android.content.Context;

import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.List;

import product.dg.xshipping.adapter.model.DistanceAndCostItem;
import product.dg.xshipping.adapter.model.GigConfirmItemList;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.adapter.model.GigItemList;
import product.dg.xshipping.model.Gig;
import product.dg.xshipping.model.GigInfo;
import product.dg.xshipping.model.Position;
import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.model.SenderInfo;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.database.model.District;
import product.dg.xshipping.service.database.model.DistrictResponse;
import product.dg.xshipping.service.database.model.GeoData;
import product.dg.xshipping.service.database.model.Ward;
import product.dg.xshipping.service.database.model.WardResponse;
import product.dg.xshipping.service.network.Network;
import product.dg.xshipping.service.network.Param;
import product.dg.xshipping.service.network.Response;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;

public class FSService {
    public interface Callback<E> {
        void onSuccess(E data);
        void onFail(String error);
    }

    private User mCurrentUser;
    private User mCurrentFacebookUser;

    private GeoData mGeoData;
    private DistrictResponse mDistrictResponse;
    private WardResponse mWardResponse;
    private Gig mGigData = new Gig();

    private int mSubIndex = 0;
    private String mCurrentRegistrationID = "";

    private static FSService instance;
    private FSService() {
        mGeoData = new GeoData();
        mDistrictResponse = new DistrictResponse();
        mWardResponse = new WardResponse();
    }

    public static FSService getInstance() {
        if (instance == null) {
            instance = new FSService();
        }

        return instance;
    }

    public void setSubIndex(int index) {
        mSubIndex = index;
    }

    public int getSubIndex() {
        return mSubIndex;
    }

    public void clearGigInfo() {
        mGigData = new Gig();
        mGeoData = new GeoData();
        mDistrictResponse = new DistrictResponse();
        mWardResponse = new WardResponse();
    }
    public void setReceiverInfo(ReceiverInfo receiverInfo) {
        mGigData.setReceiverInfo(receiverInfo);
    }
    public ReceiverInfo getReceiverInfo() {
        return mGigData.getReceiverInfo();
    }
    public void setSenderInfo(SenderInfo senderInfo) {
        mGigData.setSenderInfo(senderInfo);
    }
    public SenderInfo getSenderInfo() {
        return mGigData.getSenderInfo();
    }
    public void setGigInfo(GigInfo gigInfo) {
        mGigData.setGigInfo(gigInfo);
    }
    public GigInfo getGigInfo() {
        return mGigData.getGigInfo();
    }

    public GeoData getGeoData() {
        return mGeoData;
    }
    public DistrictResponse getDistrictData() {
        return mDistrictResponse;
    }
    public WardResponse getWardData() {
        return mWardResponse;
    }
    public void setDistrictResponse(DistrictResponse districtResponse) {
        mDistrictResponse = districtResponse;
    }
    public void setWardResponse(WardResponse wardResponse) {
        mWardResponse = wardResponse;
    }

    public User getCurrentUser() {
        return mCurrentUser;
    }

    public void setCurrentUser(User user) {
        this.mCurrentUser = user;
    }

    public void logout() {
        this.mCurrentUser = null;
        this.mCurrentFacebookUser = null;

        LoginManager.getInstance().logOut();
    }

    public User getCurrentFacebookUser() {
        return mCurrentFacebookUser;
    }

    public void setCurrentFacebookUser(User user) {
        this.mCurrentFacebookUser = user;
    }

    public boolean isLogin() {
        return mCurrentUser != null || mCurrentFacebookUser != null;
    }
    public boolean isFacebookLogin() {
        return mCurrentFacebookUser != null;
    }

    Network network = Network.getInstance();

    public void login(List<Param> params, final Callback<User> callback) {

        params.add(new Param("_request", "login"));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void register(List<Param> params, final Callback<User> callback) {

        params.add(new Param("_request", "register"));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void verify(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "verify"));
        params.add(new Param("_token", currentUser.getToken()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void loginFacebook(List<Param> params, final Callback<User> callback) {
        params.add(new Param("_request", "loginface"));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void update_profile(List<Param> params, final Callback<User> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "update_profile"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void change_password(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "change_password"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void update_avatar(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "upload_image"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getDistricts(List<Param> params, final Callback<DistrictResponse> callback) {
        params.add(new Param("_request", "getdistricts"));
        network.execute(FS.WEB_API_GET_DATA, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    DistrictResponse districts = gson.fromJson(response.getResponseObject(), DistrictResponse.class);
                    if (districts != null) {
                        callback.onSuccess(districts);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getWards(List<Param> params, final Callback<WardResponse> callback) {
        params.add(new Param("_request", "getwards"));
        network.execute(FS.WEB_API_GET_DATA, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    WardResponse wards = gson.fromJson(response.getResponseObject(), WardResponse.class);
                    if (wards != null) {
                        callback.onSuccess(wards);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getGigTicket(List<Param> params, final Callback<GigItemList> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "get_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    GigItemList gigItemList = gson.fromJson(response.getResponseObject(), GigItemList.class);
                    if (gigItemList != null) {
                        callback.onSuccess(gigItemList);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getGigConfirmTicket(List<Param> params, final Callback<GigConfirmItemList> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "get_gig_confirm_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    GigConfirmItemList gigItemList = gson.fromJson(response.getResponseObject(), GigConfirmItemList.class);
                    if (gigItemList != null) {
                        callback.onSuccess(gigItemList);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void confirmFromBookWaiting(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "confirm_from_book_waiting"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void confirmFromWaitingPick(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "confirm_from_waiting_pick"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getGigTicketDetail(List<Param> params, final Callback<GigDetailItem> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "get_gig_ticket_detail"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    GigDetailItem gigDetailItem = gson.fromJson(response.getResponseObject(), GigDetailItem.class);
                    if (gigDetailItem != null) {
                        callback.onSuccess(gigDetailItem);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void deleteGigTicket(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "delete_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void cancelGigTicket(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "cancel_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updatingGigTicket(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "updating_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void endUpdateGigTicket(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "end_update_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void createGigTicket(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "create_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getDistanceAndCost(List<Param> params, final Callback<DistanceAndCostItem> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "get_distance_and_cost"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    DistanceAndCostItem distanceAndCostItem = gson.fromJson(response.getResponseObject(), DistanceAndCostItem.class);
                    if (distanceAndCostItem != null) {
                        callback.onSuccess(distanceAndCostItem);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updateGigTicket(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "update_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getGigTicketPosition(List<Param> params, final Callback<Position> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "get_gig_position"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    Position position = gson.fromJson(response.getResponseObject(), Position.class);
                    if (position != null) {
                        callback.onSuccess(position);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updateRegistrationID(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        mCurrentRegistrationID = FirebaseInstanceId.getInstance().getToken();

        params.add(new Param("_request", "update_registration_id"));
        params.add(new Param("_registration_id", mCurrentRegistrationID));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void saveUserCredential(Context context, String username, String password) {
        Utils.saveValue(context, FS.SP_USERNAME_KEY, username);
        Utils.saveValue(context, FS.SP_PASSWORD_KEY, password);
    }

    public String getUsernameCredential(Context context) {
        return Utils.getValue(context, FS.SP_USERNAME_KEY);
    }

    public String getPasswordCredential(Context context) {
        return Utils.getValue(context, FS.SP_PASSWORD_KEY);
    }

    public void setFirstTimeOpenApp(Context context) {
        Utils.saveValue(context, FS.SP_FIST_TIME, FS.SP_FIST_TIME_VALUE);
    }

    public boolean isFirstTimeOpenApp(Context context) {
        String isFirst = Utils.getValue(context, FS.SP_FIST_TIME);
        if (isFirst.equalsIgnoreCase(FS.SP_FIST_TIME_VALUE)) {
            return false;
        }

        return true;
    }
}
