package product.dg.xshipping.service.network;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import product.dg.xshipping.MainActivity;

public class Network {

    public interface Callback {
        public void onCallBack(Response response);
        public void onFail(String error);
    }

    private static Network instance;
    private Network() {}

    public static Network getInstance() {
        if (instance == null) {
            instance = new Network();
        }
        return instance;
    }

    OkHttpClient client = new OkHttpClient();
    public void execute(String link, List<Param> params, final Callback callback) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        for (Param p : params) {
            builder.addFormDataPart(p.key, p.value);
        }
        RequestBody requestBody = builder.build();

        Request request = new Request.Builder()
                .url(link)
                .method("POST", RequestBody.create(null, new byte[0]))
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFail(e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                Gson gson = new Gson();
                String body = response.body().string();
                Response _response = gson.fromJson(body, Response.class);

                if (_response.getResponseObject().trim().equalsIgnoreCase("4213")) {
                    onTokenFail();
                }
                else {
                    callback.onCallBack(_response);
                }
            }
        });
    }

    void onTokenFail() {
        MainActivity mainActivity = MainActivity.getInstance();
        if (mainActivity != null) {
            mainActivity.tokenFail();
        }
    }
}
