package product.dg.xshipping.service.database.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class District {
    public static final String TABLE_NAME = "district";
    public static final String DISTRICT_ID = "districtid";
    public static final String DISTRICT_NAME = "name";
    public static final String DISTRICT_TYPE = "type";
    public static final String DISTRICT_LOCATION = "location";
    public static final String PROVINCE_ID = "provinceid";

    public static final String CREATE_COMMAND =
            "CREATE TABLE IF NOT EXISTS " +TABLE_NAME + "(" +
                    DISTRICT_ID + " TEXT NOT NULL PRIMARY KEY," +
                    DISTRICT_NAME + " TEXT NOT NULL," +
                    DISTRICT_TYPE + " TEXT NOT NULL," +
                    DISTRICT_LOCATION + " TEXT NOT NULL," +
                    PROVINCE_ID + " TEXT NOT NULL)";

    public static final String DROP_COMMAND =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    @SerializedName("districtid")
    String mID;
    @SerializedName("name")
    String mName;
    @SerializedName("type")
    String mType;
    @SerializedName("location")
    String mLocation;
    @SerializedName("provinceid")
    String mProvinceID;

    List<Ward> mWards;


    public String getID() {
        return mID;
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getProvinceID() {
        return mProvinceID;
    }

    public List<Ward> getListWard() {
        return mWards;
    }

    public Ward getWardByID(String id) {
        for (Ward ward : mWards) {
            if (ward.getID().equalsIgnoreCase(id)) {
                return ward;
            }
        }
        return null;
    }

    public void setWard(List<Ward> wards) {
        this.mWards = wards;
    }

    public District() {}
}
