package product.dg.xshipping.service.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.security.Provider;

import product.dg.xshipping.service.database.model.District;
import product.dg.xshipping.service.database.model.Province;
import product.dg.xshipping.service.database.model.Ward;

public class DGHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "xshipping.db";

    public DGHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Province.CREATE_COMMAND);
        sqLiteDatabase.execSQL(District.CREATE_COMMAND);
        sqLiteDatabase.execSQL(Ward.CREATE_COMMAND);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(Province.DROP_COMMAND);
        sqLiteDatabase.execSQL(District.DROP_COMMAND);
        sqLiteDatabase.execSQL(Ward.DROP_COMMAND);
        onCreate(sqLiteDatabase);
    }

    public boolean insertProvince (Province province) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Province.PROVINCE_ID, province.getID());
        contentValues.put(Province.PROVINCE_NAME, province.getName());
        contentValues.put(Province.PROVINCE_TYPE, province.getType());
        db.insert(Province.TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertDistrict (District district) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(District.DISTRICT_ID, district.getID());
        contentValues.put(District.DISTRICT_NAME, district.getName());
        contentValues.put(District.DISTRICT_TYPE, district.getType());
        contentValues.put(District.DISTRICT_LOCATION, district.getLocation());
        contentValues.put(District.PROVINCE_ID, district.getProvinceID());
        db.insert(District.TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertWard (Ward ward) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Ward.WARD_ID, ward.getID());
        contentValues.put(Ward.WARD_NAME, ward.getName());
        contentValues.put(Ward.WARD_TYPE, ward.getType());
        contentValues.put(Ward.WARD_LOCATION, ward.getLocation());
        contentValues.put(Ward.DISTRICT_ID, ward.getDistrictID());
        db.insert(Ward.TABLE_NAME, null, contentValues);
        return true;
    }
}
