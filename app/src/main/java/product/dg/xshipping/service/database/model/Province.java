package product.dg.xshipping.service.database.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Province {
    public static final String TABLE_NAME = "province";
    public static final String PROVINCE_ID = "provinceid";
    public static final String PROVINCE_NAME = "name";
    public static final String PROVINCE_TYPE = "type";

    public static final String CREATE_COMMAND =
            "CREATE TABLE IF NOT EXISTS " +TABLE_NAME + "(" +
                    PROVINCE_ID + " TEXT NOT NULL PRIMARY KEY," +
                    PROVINCE_NAME + " TEXT NOT NULL," +
                    PROVINCE_TYPE + " TEXT NOT NULL)";

    public static final String DROP_COMMAND =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    @SerializedName("provinceid")
    String mID;
    @SerializedName("name")
    String mName;
    @SerializedName("type")
    String mType;

    List<District> mDistricts;

    public String getID() {
        return mID;
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }

    public void setID(String id) {
        this.mID = id;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public List<District> getListDistrict() {
        return mDistricts;
    }

    public District getDistrictByID(String id) {
        for (District district : mDistricts) {
            if (district.getID().equalsIgnoreCase(id)){
                return district;
            }
        }
        return null;
    }

    public void setDistricts(List<District> districts) {
        this.mDistricts = districts;
    }

    public Province() {}
    public Province(String id, String name, String type) {
        mID = id;
        mName = name;
        mType = type;
    }
}
