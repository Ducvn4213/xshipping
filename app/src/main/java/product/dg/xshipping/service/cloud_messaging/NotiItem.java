package product.dg.xshipping.service.cloud_messaging;

import com.google.gson.annotations.SerializedName;

public class NotiItem {
    @SerializedName("title")
    String mTitle;
    @SerializedName("message")
    String mMessage;

    public NotiItem() {}

    public String getTitle() {
        return mTitle;
    }

    public String getMessage() {
        return mMessage;
    }

}
