package product.dg.xshipping.service.network;

import com.google.gson.annotations.SerializedName;

public class Response {
    @SerializedName("result")
    boolean mResult;
    @SerializedName("data")
    String mResponseObject;

    public boolean getResult() {
        return mResult;
    }

    public String getResponseObject() {
        return mResponseObject;
    }
}
