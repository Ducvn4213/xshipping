package product.dg.xshipping.get_location;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import product.dg.xshipping.R;
import product.dg.xshipping.service.TrackerGPS;

public class GetLocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.f_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        LatLng defaultLocation = getCurrentLocationLatLng();
        mMap.addMarker(new MarkerOptions().position(defaultLocation).title(getString(R.string.get_current_location_your_location)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
    }

    private LatLng getCurrentLocationLatLng() {
        LatLng defaultLocation = new LatLng(0, 0);
        TrackerGPS trackerGPS = new TrackerGPS(GetLocationActivity.this);
        if (trackerGPS.canGetLocation()) {
            if (trackerGPS.getLatitude() == 0 && trackerGPS.getLongitude() == 0) {
                return getCurrentLocationLatLng();
            }
            defaultLocation = new LatLng(trackerGPS.getLatitude(), trackerGPS.getLongitude());
            return defaultLocation;
        }

        return defaultLocation;
    }
}
