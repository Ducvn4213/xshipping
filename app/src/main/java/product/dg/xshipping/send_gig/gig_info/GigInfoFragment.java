package product.dg.xshipping.send_gig.gig_info;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;

public class GigInfoFragment extends Fragment implements CalendarDatePickerDialogFragment.OnDateSetListener, GigInfoInterfaces.RequiredViewOps {
    private DateTimeFormatter mFormatter;
    private GigInfoInterfaces.ProvidedPresenterOps mPresenter;

    Button mBack;
    Button mNext;
    EditText mName;
    EditText mWeight;
    EditText mCod;
    EditText mNote;
    AppCompatSpinner mPaymentMethod;
    AppCompatSpinner mGigSize;
    AppCompatSpinner mCOD;
    Button mPickupDate;
    List<String> mPaymentMethodList = new ArrayList<>();
    List<String> mGigSizeList = new ArrayList<>();
    List<String> mCODList = new ArrayList<>();

    private static GigInfoFragment instance;

    public static GigInfoFragment getInstance() {
        return instance;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gig_info, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        instance = GigInfoFragment.this;
    }

    public void init() {
        mFormatter = DateTimeFormat.forPattern(getString(R.string.date_pattern));

        GigInfoPresenter presenter = new GigInfoPresenter(this);
        GigInfoModel model = new GigInfoModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;

        //Payment Method
        String[] items = getResources().getStringArray(R.array.array_payment_method);
        if (mPaymentMethodList.size() == 0) {
            Collections.addAll(mPaymentMethodList, items);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mPaymentMethodList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mPaymentMethod.setAdapter(pm_adapter);

        //Gig Size
        items = getResources().getStringArray(R.array.array_gig_size);
        if (mGigSizeList.size() == 0) {
            Collections.addAll(mGigSizeList, items);
        }

        ArrayAdapter<String> gs_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mGigSizeList);
        gs_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mGigSize.setAdapter(gs_adapter);

        //Cod
        items = getResources().getStringArray(R.array.array_cod);
        if (mCODList.size() == 0) {
            Collections.addAll(mCODList, items);
        }

        ArrayAdapter<String> cod_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mCODList);
        cod_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mCOD.setAdapter(cod_adapter);

        //set default date
        DateTime dateTime = new DateTime();
        String prefix = getString(R.string.send_gigs_pickup_date);
        mPickupDate.setText(prefix + ": " + mFormatter.print(dateTime.getMillis()));
    }

    public void bindingControls(View view) {
        mBack = (Button) view.findViewById(R.id.btn_back);
        mNext = (Button) view.findViewById(R.id.btn_next);
        mPaymentMethod = (AppCompatSpinner) view.findViewById(R.id.spn_payment_method);
        mGigSize = (AppCompatSpinner) view.findViewById(R.id.spn_gigs_size);
        mCOD = (AppCompatSpinner) view.findViewById(R.id.spn_cod);
        mPickupDate = (Button) view.findViewById(R.id.btn_pickup_date);
        mName = (EditText) view.findViewById(R.id.edt_gig_name);
        mWeight = (EditText) view.findViewById(R.id.edt_weight);
        mCod = (EditText) view.findViewById(R.id.edt_cod);
        mNote = (EditText) view.findViewById(R.id.edt_note);
    }

    public void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.onBack();
            }
        });
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FSService.getInstance().isLogin() == false) {
                    final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                            .setTitle(getString(R.string.dialog_notice_title))
                            .setMessage(getString(R.string.dialog_message_login_required))
                            .setPositiveButton(R.string.dialog_button_login_now, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(getParentActivity(), LoginActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .create();
                    if (!dialog.isShowing()) {
                        dialog.show();
                    }
                    return;
                }

                User currentUser = FSService.getInstance().isFacebookLogin() ? FSService.getInstance().getCurrentFacebookUser() : FSService.getInstance().getCurrentUser();
                if (!currentUser.isActive()) {
                    final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                            .setTitle(getString(R.string.dialog_notice_title))
                            .setMessage(getString(R.string.dialog_message_active_required))
                            .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .create();
                    if (!dialog.isShowing()) {
                        dialog.show();
                    }
                    return;
                }

                String name = mName.getText().toString();
                String weight = mWeight.getText().toString();
                String payment_method = mPaymentMethod.getSelectedItem().toString();
                String size = mGigSize.getSelectedItem().toString();
                String date = mPickupDate.getText().toString();
                String cod = mCod.getText().toString();
                String note = mNote.getText().toString();

                if (mCod.isEnabled()) {
                    mPresenter.onNext(name, weight, payment_method, size, date, cod, note);
                }
                else {
                    mPresenter.onNext(name, weight, payment_method, size, date, note);
                }

            }
        });

        //Pickup Date
        mPickupDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar start = Calendar.getInstance();
                DateTime date = (new DateTime()).withTime(0, 0, 0, 0);
                start.setTimeInMillis(date.getMillis());

                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(GigInfoFragment.this)
                        .setFirstDayOfWeek(Calendar.MONDAY)
                        .setPreselectedDate(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH))
                        .setDoneText(getString(R.string.dialog_button_ok))
                        .setCancelText(getString(R.string.dialog_button_cancel))
                        .setThemeLight();
                cdp.show(GigInfoFragment.this.getFragmentManager(), FS.DATE_PICKER_START_DIALOG_TAG);
            }
        });

        mCOD.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    mCod.setText("");
                    mCod.setEnabled(false);
                    mPresenter.disablePaymentInfo();
                }
                else {
                    mCod.setEnabled(true);
                    mCod.requestFocus();
                    mPresenter.enablePaymentInfo();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        if (dialog.getTag().equalsIgnoreCase(FS.DATE_PICKER_START_DIALOG_TAG)) {
            DateTime dateTime = (new DateTime()).withTime(0, 0, 0, 0);
            dateTime = dateTime.withDate(year, monthOfYear + 1, dayOfMonth);

            DateTime newDateTime = new DateTime();
            newDateTime = newDateTime.withTime(0, 0, 0, 0);
            if ((newDateTime.getMillis() - dateTime.getMillis()) > 0) {
                showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_date_wrong);
                return;
            }

            newDateTime = new DateTime();
            if (newDateTime.getHourOfDay() >= 16) {
                newDateTime = newDateTime.withTime(0, 0, 0, 0);
                if ((newDateTime.getMillis() - dateTime.getMillis()) == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_date_late);
                    return;
                }
            }

            String prefix = getString(R.string.send_gigs_pickup_date);
            mPickupDate.setText(prefix + ": " + mFormatter.print(dateTime.getMillis()));
        }
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) GigInfoFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public void clearData() {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPaymentMethod.setSelection(0);
                mGigSize.setSelection(0);
                mCOD.setSelection(0);
                mName.setText("");
                mWeight.setText("");
                mCod.setText("");
                mNote.setText("");
            }
        });
    }
}
