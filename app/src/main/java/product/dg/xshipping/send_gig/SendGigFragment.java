package product.dg.xshipping.send_gig;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import product.dg.xshipping.R;
import product.dg.xshipping.send_gig.gig_info.GigInfoFragment;
import product.dg.xshipping.send_gig.receiver_info.ReceiverInfoFragment;
import product.dg.xshipping.send_gig.sender_info.SenderInfoFragment;
import product.dg.xshipping.util.FS;

public class SendGigFragment extends Fragment implements ViewPager.OnPageChangeListener{


    SmartTabLayout mTab;
    ViewPager mViewPager;

    FragmentPagerItemAdapter mAdapter;

    private static SendGigFragment instance;
    public static SendGigFragment getInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send_gig, container, false);

        bindingUI(view);
        setupUIData();
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        instance = SendGigFragment.this;
    }

    private void bindingUI(View view) {
        mTab = (SmartTabLayout) view.findViewById(R.id.stl_tab);
        mViewPager = (ViewPager) view.findViewById(R.id.vp_container);
    }

    private void setupUIData() {
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getContext());

        Bundle receiver = new Bundle();
        receiver.putString(FS.EXTRA_SEND_GIG, FS.RECEIVER_INFO);
        creator.add(R.string.send_gigs_receiver_info_title, ReceiverInfoFragment.class, receiver);

        Bundle gig = new Bundle();
        gig.putString(FS.EXTRA_SEND_GIG, FS.GIG_INFO);
        creator.add(R.string.send_gigs_gig_info_title, GigInfoFragment.class, gig);

        Bundle sender = new Bundle();
        sender.putString(FS.EXTRA_SEND_GIG, FS.SENDER_INFO);
        creator.add(R.string.send_gigs_sender_info_title, SenderInfoFragment.class, gig);

        mAdapter = new FragmentPagerItemAdapter(getChildFragmentManager(),
                creator.create());

        mViewPager.setAdapter(mAdapter);
        mTab.setCustomTabView(R.layout.layout_smart_tab, R.id.tv_ticket_tab_item);
        mTab.setViewPager(mViewPager);
        mTab.getTabAt(0).setEnabled(false);
        mTab.getTabAt(1).setEnabled(false);
        mTab.getTabAt(2).setEnabled(false);

        mViewPager.addOnPageChangeListener(this);
    }

    private void setupControlEvents() {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("","");
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d("","");
    }

    public void gotoTab(int index) {
        mViewPager.setCurrentItem(index);
    }

    public Fragment getTab(int index) {
        return mAdapter.getPage(index);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        android.support.v4.app.Fragment fragment = getParentFragment().getFragmentManager().findFragmentById(R.id.vp_container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
