package product.dg.xshipping.send_gig.sender_info;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.TrackerGPS;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;

import static android.app.Activity.RESULT_OK;


public class SenderInfoFragment extends Fragment implements SenderInfoInterfaces.RequiredViewOps {

    private SenderInfoInterfaces.ProvidedPresenterOps mPresenter;

    Button mBack;
    Button mFinish;
    AppCompatSpinner mAddressSpinner;
    AppCompatSpinner mBankSpinner;
    List<String> mAddressSpinnerList = new ArrayList<>();
    List<String> mBankSpinnerList = new ArrayList<>();
    EditText mName;
    EditText mPhone;
    EditText mAddress;
    EditText mPaymentName;
    EditText mPaymentNumber;
    EditText mPaymentBank;
    TextView mBankInfo;
    TextView mBankInfo2;
    LinearLayout mPaymentContainer;

    TrackerGPS mTrackerGPS;

    ProgressDialog mProgressDialog;

    boolean mPreventAddress = true;
    boolean mPreventBank = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sender_info, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadOldData();
    }

    public void init() {
        SenderInfoPresenter presenter = new SenderInfoPresenter(this);
        SenderInfoModel model = new SenderInfoModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;

        //Address Spinner
        String[] items = getResources().getStringArray(R.array.array_address);
        if (mAddressSpinnerList.size() == 0) {
            Collections.addAll(mAddressSpinnerList, items);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mAddressSpinnerList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mAddressSpinner.setAdapter(pm_adapter);


        //Bank Spinner
        String[] items2 = getResources().getStringArray(R.array.array_bank);
        if (mBankSpinnerList.size() == 0) {
            Collections.addAll(mBankSpinnerList, items2);
        }

        ArrayAdapter<String> b_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mBankSpinnerList);
        b_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mBankSpinner.setAdapter(b_adapter);
    }

    public void bindingControls(View view) {
        mBack = (Button) view.findViewById(R.id.btn_back);
        mFinish = (Button) view.findViewById(R.id.btn_finish);
        mAddressSpinner = (AppCompatSpinner) view.findViewById(R.id.spn_address);
        mBankSpinner = (AppCompatSpinner) view.findViewById(R.id.spn_bank);
        mName = (EditText) view.findViewById(R.id.edt_sender_name);
        mPhone = (EditText) view.findViewById(R.id.edt_sender_phone);
        mAddress = (EditText) view.findViewById(R.id.edt_address);
        mBankInfo = (TextView) view.findViewById(R.id.tv_bank_info);
        mBankInfo2 = (TextView) view.findViewById(R.id.tv_bank_info2);
        mPaymentName = (EditText) view.findViewById(R.id.edt_sender_bank_account_name);
        mPaymentNumber = (EditText) view.findViewById(R.id.edt_sender_bank_account_number);
        mPaymentBank = (EditText) view.findViewById(R.id.edt_sender_bank_account_bank);
        mPaymentContainer = (LinearLayout) view.findViewById(R.id.payment_container);
    }

    public void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mName.getText().toString();
                String phone = mPhone.getText().toString();
                String address = mAddress.getText().toString();
                String bank_name = mPaymentName.getText().toString();
                String bank_number = mPaymentNumber.getText().toString();
                String bank_branch = mPaymentBank.getText().toString();

                mPresenter.saveData(name, phone, address, bank_name, bank_number, bank_branch);
                mPresenter.onBack();
                mPreventAddress = true;
                mPreventBank = true;
            }
        });

        mFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FSService.getInstance().isLogin() == false) {
                    final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                            .setTitle(getString(R.string.dialog_notice_title))
                            .setMessage(getString(R.string.dialog_message_login_required))
                            .setPositiveButton(R.string.dialog_button_login_now, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(getParentActivity(), LoginActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .create();
                    if (!dialog.isShowing()) {
                        dialog.show();
                    }
                    return;
                }

                String name = mName.getText().toString();
                String phone = mPhone.getText().toString();
                String address = mAddress.getText().toString();

                if (mPaymentContainer.getVisibility() == View.VISIBLE) {
                    String bank_name = mPaymentName.getText().toString();
                    String bank_number = mPaymentNumber.getText().toString();
                    String bank_bank = mPaymentBank.getText().toString();

                    mPresenter.onFinish(name, phone, address, bank_name, bank_number, bank_bank);
                }
                else {
                    mPresenter.onFinish(name, phone, address);
                }
            }
        });

        mAddressSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mPreventAddress) {
                    mPreventAddress = false;
                    return;
                }
                if (i == 0) {
                    User currentUser = FSService.getInstance().isFacebookLogin() ? FSService.getInstance().getCurrentFacebookUser() : FSService.getInstance().getCurrentUser();
                    if (currentUser != null) {
                        mAddress.setText(currentUser.getAddress());
                    }
                }
                else if (i == 1){
                    checkLocationPermission();
                }
                else {
                    try {
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                        startActivityForResult(builder.build(getParentActivity()), FS.PICK_LOCATION_REQUEST_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO
            }
        });

        mBankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mPreventBank) {
                    mPreventBank = false;
                    return;
                }
                if (i == 0) {
                    User currentUser = FSService.getInstance().isFacebookLogin() ? FSService.getInstance().getCurrentFacebookUser() : FSService.getInstance().getCurrentUser();
                    if (currentUser != null) {
                        mPaymentName.setText(currentUser.getBankName());
                        mPaymentNumber.setText(currentUser.getBankNumber());
                        mPaymentBank.setText(currentUser.getBankBranch());

                        mPaymentName.setEnabled(false);
                        mPaymentNumber.setEnabled(false);
                        mPaymentBank.setEnabled(false);
                    }
                }
                else {
                    mPaymentName.setText("");
                    mPaymentNumber.setText("");
                    mPaymentBank.setText("");

                    mPaymentName.setEnabled(true);
                    mPaymentNumber.setEnabled(true);
                    mPaymentBank.setEnabled(true);

                    mPaymentName.requestFocus();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO
            }
        });
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) SenderInfoFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void loadOldData(String name, String phone, String address, String bank_name, String bank_number, String bank_branch) {
        mName.setText(name);
        mPhone.setText(phone);
        mAddress.setText(address);
        mPaymentName.setText(bank_name);
        mPaymentNumber.setText(bank_number);
        mPaymentBank.setText(bank_branch);
    }

    @Override
    public void presentDistanceAndCost(String distance, String cost) {
        int intDistance = Integer.parseInt(distance);
        int du = intDistance % 1000 > 0 ? 1 : 0;
        int reDistance = intDistance/1000 + du;

        if (intDistance > 50000) {
            showDialog(R.string.dialog_notice_title, R.string.send_gigs_gig_over_distance);
            return;
        }

        String mess = "Quãng đường gửi: " + reDistance + "km, chi phí: " + cost + " VNĐ \r\nBạn có đồng ý tiếp tục?";
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(R.string.dialog_notice_title))
                .setMessage(mess)
                .setNegativeButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.createGigTicket();
                    }
                })
                .setPositiveButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void checkLocationPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(getParentActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        else {
            doFillCurrentAddress();
            return;
        }

        if (ContextCompat.checkSelfPermission(getParentActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        else {
            doFillCurrentAddress();
            return;
        }

        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(getParentActivity(), perArr, FS.PERMISSIONS_REQUEST);
    }

    private void doFillCurrentAddress() {
        mTrackerGPS = new TrackerGPS(getParentActivity());
        if(mTrackerGPS.canGetLocation()){

            double longitude = mTrackerGPS.getLongitude();
            double latitude = mTrackerGPS.getLatitude();

            if (longitude == 0 && latitude == 0) {
                doFillCurrentAddress();
                return;
            }

            Geocoder geocoder = new Geocoder(getParentActivity());
            try {
                Address address = geocoder.getFromLocation(latitude, longitude, 1).get(0);
                String addressString = Utils.getStringAddressFromAddress(address);
                mAddress.setText(addressString);
            } catch (IOException e) {
                showDialog(R.string.dialog_title_error, R.string.dialog_message_get_current_location_fail);
            }
        }
        else
        {
            mTrackerGPS.showSettingsAlert();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == FS.PERMISSIONS_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    doFillCurrentAddress();
                }
                else {
                    showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_location_permission);
                }
            }
            else {
                showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_location_permission);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FS.PICK_LOCATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String addressString = PlacePicker.getPlace(data, getParentActivity()).getAddress().toString();
                String name = mName.getText().toString();
                String phone = mPhone.getText().toString();
                String bank_name = mPaymentName.getText().toString();
                String bank_number = mPaymentNumber.getText().toString();
                String bank_branch = mPaymentBank.getText().toString();

                mPresenter.saveData(name, phone, addressString, bank_name, bank_number, bank_branch);
                mAddress.setText(addressString);
            }
        }
    }

    public void enablePaymentInformation() {
        mPaymentContainer.setVisibility(View.VISIBLE);
    }

    public void disablePaymentInformation() {
        mPaymentContainer.setVisibility(View.GONE);
    }
}
