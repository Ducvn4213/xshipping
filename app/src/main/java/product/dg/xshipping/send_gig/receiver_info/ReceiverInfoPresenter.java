package product.dg.xshipping.send_gig.receiver_info;

import android.widget.ArrayAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.send_gig.SendGigFragment;
import product.dg.xshipping.service.database.model.DistrictResponse;
import product.dg.xshipping.service.database.model.WardResponse;

public class ReceiverInfoPresenter implements ReceiverInfoInterfaces.ProvidedPresenterOps, ReceiverInfoInterfaces.RequiredPresenterOps {
    private WeakReference<ReceiverInfoInterfaces.RequiredViewOps> mView;
    private ReceiverInfoInterfaces.ProvidedModelOps mModel;

    ReceiverInfoPresenter(ReceiverInfoInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(ReceiverInfoInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void onNext(String name, String phone, String address, String province, String district, String ward) {
        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_name_missing);
            return;
        }

        if (phone.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_phone_missing);
            return;
        }

        if (phone.startsWith("09")) {
            if (phone.length() != 10) {
                mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_phone_wrong);
                return;
            }
        }

        if (phone.startsWith("01")) {
            if (phone.length() != 11) {
                mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_phone_wrong);
                return;
            }
        }

        if (address.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_address_missing);
            return;
        }

        if (address.matches(".*\\d+.*") == false || address.matches(".*[a-zA-Z]+.*") == false) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_address_wrong);
            return;
        }

        mModel.onNext(name, phone, address, province, district, ward);
    }

    @Override
    public void onSelectProvince(int id) {
        mModel.loadDistricts(id);
    }

    @Override
    public void onSelectDistrict(int id) {
        mModel.loadWards(id);
    }

    @Override
    public void loadOldInfo() {
        mModel.loadOldData();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void moveNext() {
        SendGigFragment.getInstance().gotoTab(1);
    }

    @Override
    public ArrayAdapter<String> createAdapterWithData(List<String> data) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mView.get().getParentActivity(), R.layout.layout_spinner_item_left,
                R.id.tv_item_text, data);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_left);
        return adapter;
    }

    @Override
    public void loadGeoData(final ArrayAdapter<String> provinces, final ArrayAdapter<String> districts, final ArrayAdapter<String> wards) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().loadGeoData(provinces, districts, wards);
            }
        });
    }

    @Override
    public void notifyChanged(final ArrayAdapter<String> districtAdapter,final ArrayAdapter<String> wardAdapter) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                districtAdapter.notifyDataSetChanged();
                wardAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void handleOnDistrictCallback(final DistrictResponse data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.handleOnDistrictCallback(data);
            }
        });
    }

    @Override
    public void handleOnWardCallback(final WardResponse data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.handleOnWardCallback(data);
            }
        });
    }

    @Override
    public void loadOldData(final String name, final String phone, final String address, final ArrayAdapter<String> provinceAdapter, final ArrayAdapter<String> districtAdapter, final ArrayAdapter<String> wardAdapter, final int province, final int district, final int ward) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().loadOldData(name, phone, address, provinceAdapter, districtAdapter, wardAdapter, province, district, ward);
            }
        });
    }
}
