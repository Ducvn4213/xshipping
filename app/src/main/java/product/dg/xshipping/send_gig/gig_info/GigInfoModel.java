package product.dg.xshipping.send_gig.gig_info;

import product.dg.xshipping.model.GigInfo;
import product.dg.xshipping.send_gig.receiver_info.ReceiverInfoInterfaces;
import product.dg.xshipping.service.FSService;

public class GigInfoModel implements GigInfoInterfaces.ProvidedModelOps {
    private GigInfoInterfaces.RequiredPresenterOps mPresenter;

    GigInfoModel(GigInfoInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    private GigInfo mGigInfo;
    private FSService mService = FSService.getInstance();

    @Override
    public void onNext(String name, String weight, String payment_method, String size, String date, String cod, String note) {
        if (mGigInfo == null) {
            mGigInfo = new GigInfo();
        }

        mGigInfo.setName(name);
        mGigInfo.setWeight(weight);
        mGigInfo.setPaymentMethod(payment_method);
        mGigInfo.setSize(size);
        mGigInfo.setDate(date);
        mGigInfo.setCod(cod);
        mGigInfo.setNote(note);

        mService.setGigInfo(mGigInfo);
        mPresenter.moveNext();
    }
}
