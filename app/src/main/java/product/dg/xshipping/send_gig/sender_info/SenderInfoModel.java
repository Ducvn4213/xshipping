package product.dg.xshipping.send_gig.sender_info;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.DistanceAndCostItem;
import product.dg.xshipping.model.GigInfo;
import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.model.SenderInfo;
import product.dg.xshipping.model.User;
import product.dg.xshipping.send_gig.gig_info.GigInfoFragment;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class SenderInfoModel implements SenderInfoInterfaces.ProvidedModelOps {
    private SenderInfoInterfaces.RequiredPresenterOps mPresenter;

    SenderInfoModel(SenderInfoInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    private SenderInfo mSenderInfo;
    private FSService mService = FSService.getInstance();


    @Override
    public void onFinish(String name, String phone, String address, String bank_name, String bank_number, String bank_bank) {
        if (mSenderInfo == null) {
            mSenderInfo = new SenderInfo();
        }

        mSenderInfo.setName(name);
        mSenderInfo.setPhone(phone);
        mSenderInfo.setAddress(address);
        mSenderInfo.setBankName(bank_name);
        mSenderInfo.setBankNumber(bank_number);
        mSenderInfo.setBankBank(bank_bank);

        mService.setSenderInfo(mSenderInfo);
        getDistanceAndCostInfo();
    }

    private void getDistanceAndCostInfo() {
        ReceiverInfo receiverInfo = mService.getReceiverInfo();
        SenderInfo senderInfo = mService.getSenderInfo();
        GigInfo gigInfo = mService.getGigInfo();

        String toAddress = receiverInfo.getAddress() + ", "
                + receiverInfo.getWard() + ", "
                + receiverInfo.getDistrict() + ", "
                + receiverInfo.getProvince();

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_from", senderInfo.getAddress()));
        paramList.add(new Param("_to", toAddress));
        if (gigInfo.getSize().trim().equalsIgnoreCase("nhỏ")) {
            paramList.add(new Param("_is_big", "false"));
        }
        else {
            paramList.add(new Param("_is_big", "true"));
        }

        mService.getDistanceAndCost(paramList, new FSService.Callback<DistanceAndCostItem>() {
            @Override
            public void onSuccess(DistanceAndCostItem data) {
                mPresenter.presentDistanceAndCost(data.getDistanse(), data.getCost());
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void createGigTicket() {
        ReceiverInfo receiverInfo = mService.getReceiverInfo();
        SenderInfo senderInfo = mService.getSenderInfo();
        GigInfo gigInfo = mService.getGigInfo();

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_name", receiverInfo.getName()));
        paramList.add(new Param("_phone", receiverInfo.getPhone()));
        paramList.add(new Param("_address", receiverInfo.getAddress()));
        paramList.add(new Param("_city", receiverInfo.getProvince()));
        paramList.add(new Param("_district", receiverInfo.getDistrict()));
        paramList.add(new Param("_ward", receiverInfo.getWard()));
        paramList.add(new Param("_sender_address", senderInfo.getAddress()));
        paramList.add(new Param("_gig_name", gigInfo.getName()));
        paramList.add(new Param("_weight", gigInfo.getWeight()));
        paramList.add(new Param("_payment_method", gigInfo.getPaymentMethod()));
        paramList.add(new Param("_size", gigInfo.getSize()));
        paramList.add(new Param("_date", gigInfo.getDate()));
        paramList.add(new Param("_cod", gigInfo.getCod()));
        paramList.add(new Param("_note", gigInfo.getNote()));
        paramList.add(new Param("_bank_name", senderInfo.getBankName()));
        paramList.add(new Param("_bank_number", senderInfo.getBankNumber()));
        paramList.add(new Param("_bank_bank", senderInfo.getBankBank()));

        mService.createGigTicket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mService.clearGigInfo();
                mPresenter.showDialog(R.string.dialog_notice_title, data);
                mPresenter.moveToFirst();
                mPresenter.moveFinish();
                GigInfoFragment.getInstance().clearData();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }


    @Override
    public SenderInfo getSenderInfo() {
        if (mService.getSenderInfo().getName() != null) {
            return mService.getSenderInfo();
        }

        if (mSenderInfo == null) {
            User currentUser = mService.isFacebookLogin() ? mService.getCurrentFacebookUser() : mService.getCurrentUser();
            if (currentUser != null) {
                mSenderInfo = new SenderInfo();
                mSenderInfo.setName(currentUser.getName());
                mSenderInfo.setPhone(currentUser.getPhone());
                mSenderInfo.setAddress(currentUser.getAddress());
            }
        }

        return mSenderInfo;
    }

    @Override
    public void saveData(String name, String phone, String address, String bank_name, String bank_number, String bank_branch) {
        if (mSenderInfo == null) {
            mSenderInfo = new SenderInfo();
        }

        mSenderInfo.setName(name);
        mSenderInfo.setPhone(phone);
        mSenderInfo.setAddress(address);
        mSenderInfo.setBankName(bank_name);
        mSenderInfo.setBankNumber(bank_number);
        mSenderInfo.setBankBank(bank_branch);


        mService.setSenderInfo(mSenderInfo);
    }
}
