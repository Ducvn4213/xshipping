package product.dg.xshipping.send_gig.gig_info;

import android.support.v7.app.AppCompatActivity;

public class GigInfoInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);
    }

    interface ProvidedPresenterOps {
        void enablePaymentInfo();
        void disablePaymentInfo();
        void onBack();
        void onNext(String name, String weight, String payment_method, String size, String date, String cod, String note);
        void onNext(String name, String weight, String payment_method, String size, String date, String note);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void moveNext();
    }

    interface ProvidedModelOps {
        void onNext(String name, String weight, String payment_method, String size, String date, String cod, String note);
    }
}
