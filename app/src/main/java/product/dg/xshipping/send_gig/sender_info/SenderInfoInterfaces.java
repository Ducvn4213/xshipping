package product.dg.xshipping.send_gig.sender_info;

import android.support.v7.app.AppCompatActivity;

import product.dg.xshipping.model.SenderInfo;

public class SenderInfoInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void loadOldData(String name, String phone, String address, String bank_name, String bank_number, String bank_branch);
        void presentDistanceAndCost(String distance, String code);
    }

    interface ProvidedPresenterOps {
        void onBack();
        void onFinish(String name, String phone, String address);
        void onFinish(String name, String phone, String address, String bank_name, String bank_number, String bank_bank);
        void loadOldData();
        void saveData(String name, String phone, String address, String bank_name, String bank_number, String bank_branch);
        void createGigTicket();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void moveFinish();
        void moveToFirst();
        void presentDistanceAndCost(String distance, String code);
    }

    interface ProvidedModelOps {
        void onFinish(String name, String phone, String address, String bank_name, String bank_number, String bank_bank);
        SenderInfo getSenderInfo();
        void saveData(String name, String phone, String address, String bank_name, String bank_number, String bank_branch);
        void createGigTicket();
    }
}
