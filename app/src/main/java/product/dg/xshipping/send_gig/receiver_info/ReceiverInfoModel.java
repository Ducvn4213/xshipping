package product.dg.xshipping.send_gig.receiver_info;

import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.database.model.District;
import product.dg.xshipping.service.database.model.DistrictResponse;
import product.dg.xshipping.service.database.model.Province;
import product.dg.xshipping.service.database.model.Ward;
import product.dg.xshipping.service.database.model.WardResponse;
import product.dg.xshipping.service.network.Param;
import product.dg.xshipping.util.Utils;

public class ReceiverInfoModel implements ReceiverInfoInterfaces.ProvidedModelOps {
    private ReceiverInfoInterfaces.RequiredPresenterOps mPresenter;

    FSService mService = FSService.getInstance();

    List<String> mWardList = new ArrayList<>();
    List<String> mDistrictList = new ArrayList<>();
    List<String> mProvinceList = new ArrayList<>();

    ArrayAdapter<String> mWardAdapter;
    ArrayAdapter<String> mDistrictAdapter;
    ArrayAdapter<String> mProvinceAdapter;

    List<Province> mProvinceDataList = new ArrayList<>();
    List<District> mDistrictDataList = new ArrayList<>();
    List<Ward> mWardDataList = new ArrayList<>();

    ReceiverInfoModel(ReceiverInfoInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;

        mProvinceDataList = mService.getGeoData().getListProvince();
        mDistrictDataList = mService.getDistrictData().getData();
        mWardDataList = mService.getWardData().getData();

        mProvinceList = mService.getGeoData().getStringListProvince();
        mProvinceDataList = mService.getGeoData().getListProvince();
        mDistrictList = mService.getDistrictData().getStringListDistrict();
        mWardList = mService.getWardData().getStringListWard();

        mProvinceAdapter = mPresenter.createAdapterWithData(mProvinceList);
        mDistrictAdapter = mPresenter.createAdapterWithData(mDistrictList);
        mWardAdapter = mPresenter.createAdapterWithData(mWardList);

        mPresenter.loadGeoData(mProvinceAdapter, mDistrictAdapter, mWardAdapter);
    }

    private ReceiverInfo mReceiverInfo;

    @Override
    public void onNext(String name, String phone, String address, String province, String district, String ward) {
        if (mReceiverInfo == null) {
            mReceiverInfo = new ReceiverInfo();
        }

        mReceiverInfo.setName(name);
        mReceiverInfo.setPhone(phone);
        mReceiverInfo.setAddress(address);
        mReceiverInfo.setProvince(province);
        mReceiverInfo.setDistrict(district);
        mReceiverInfo.setWard(ward);

        mService.setReceiverInfo(mReceiverInfo);
        mPresenter.moveNext();
    }

    @Override
    public ReceiverInfo getReceiverInfo() {
        return mReceiverInfo;
    }

    @Override
    public void loadDistricts(int province_id) {
        if (province_id == 0) {
            mPresenter.handleOnDistrictCallback(new DistrictResponse());
            return;
        }

        province_id--;
        String province = mProvinceDataList.get(province_id).getID();
        List<Param> params = new ArrayList<>();
        params.add(new Param("_id", province));

        mService.getDistricts(params, new FSService.Callback<DistrictResponse>() {
            @Override
            public void onSuccess(DistrictResponse data) {
                mService.setDistrictResponse(data);
                mPresenter.handleOnDistrictCallback(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_district_fail);
            }
        });
    }

    @Override
    public void loadWards(int district_id) {
        if (district_id == 0) {
            mPresenter.handleOnWardCallback(new WardResponse());
            return;
        }

        district_id--;
        String district = mDistrictDataList.get(district_id).getID();
        List<Param> params = new ArrayList<>();
        params.add(new Param("_id", district));

        mService.getWards(params, new FSService.Callback<WardResponse>() {
            @Override
            public void onSuccess(WardResponse data) {
                mService.setWardResponse(data);
                mPresenter.handleOnWardCallback(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_ward_fail);
            }
        });
    }

    @Override
    public void handleOnDistrictCallback(DistrictResponse data) {
        mDistrictDataList = data.getData();
        mDistrictList = data.getStringListDistrict();
        mDistrictAdapter.clear();
        mDistrictAdapter.addAll(mDistrictList);
        if (mDistrictList.size() > 0) {
            mWardList.clear();
            mWardList = new WardResponse().getStringListWard();
            mWardAdapter.clear();
            mWardAdapter.addAll(mWardList);
            mPresenter.notifyChanged(mDistrictAdapter, mWardAdapter);
        }
        else {
            mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_district_fail);
        }
    }

    @Override
    public void handleOnWardCallback(WardResponse data) {
        mWardDataList = data.getData();
        mWardList = data.getStringListWard();
        mWardAdapter.clear();
        mWardAdapter.addAll(mWardList);
        if (mWardList.size() > 0) {
            mPresenter.notifyChanged(mDistrictAdapter, mWardAdapter);
        }
        else {
            mPresenter.showDialog(R.string.dialog_title_error, R.string.send_gigs_receiver_message_load_ward_fail);
        }
    }

    @Override
    public void loadOldData() {
        mReceiverInfo = mService.getReceiverInfo();

        String name = mReceiverInfo.getName();
        String phone = mReceiverInfo.getPhone();
        String address = mReceiverInfo.getAddress();
        int province = Utils.getPositionByName(mProvinceList, mReceiverInfo.getProvince());
        int district = Utils.getPositionByName(mDistrictList, mReceiverInfo.getDistrict());
        int ward = Utils.getPositionByName(mWardList, mReceiverInfo.getWard());
        mPresenter.loadOldData(name, phone, address, mProvinceAdapter, mDistrictAdapter, mWardAdapter, province, district, ward);
    }
}
