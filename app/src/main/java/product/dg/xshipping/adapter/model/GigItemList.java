package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GigItemList {
    @SerializedName("data")
    List<GigItem> mData;

    public GigItemList() {}

    public List<GigItem> getData() {
        return mData;
    }
}
