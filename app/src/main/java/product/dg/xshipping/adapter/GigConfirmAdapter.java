package product.dg.xshipping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigConfirmItem;
import product.dg.xshipping.adapter.model.GigItem;

public class GigConfirmAdapter extends ArrayAdapter<GigConfirmItem> {

    private Context mContext;
    private List<GigConfirmItem> mData;
    private static LayoutInflater mInflater = null;

    public GigConfirmAdapter(Context context, List<GigConfirmItem> data) {
        super(context, R.layout.layout_manage_gig_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_manage_confirm_gig_item, null);
        }


        TextView title = (TextView) view.findViewById(R.id.edt_name);
        TextView size = (TextView) view.findViewById(R.id.edt_size);
        TextView from = (TextView) view.findViewById(R.id.edt_from);
        TextView to = (TextView) view.findViewById(R.id.edt_to);
        TextView status = (TextView) view.findViewById(R.id.edt_status);

        GigConfirmItem data = mData.get(position);

        title.setText(data.getTitle());
        size.setText(data.getSize());
        from.setText("From: " + data.getFrom());
        to.setText("To: " + data.getTo());
        status.setText(data.getShipper() + " muốn giao đơn hàng này");

        return view;
    }
}
