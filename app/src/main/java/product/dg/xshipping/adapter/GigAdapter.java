package product.dg.xshipping.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigItem;

public class GigAdapter extends ArrayAdapter<GigItem> {

    private Context mContext;
    private List<GigItem> mData;
    private static LayoutInflater mInflater = null;

    public GigAdapter(Context context, List<GigItem> data) {
        super(context, R.layout.layout_manage_gig_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_manage_gig_item, null);
        }


        TextView title = (TextView) view.findViewById(R.id.edt_name);
        TextView size = (TextView) view.findViewById(R.id.edt_size);
        TextView from = (TextView) view.findViewById(R.id.edt_from);
        TextView to = (TextView) view.findViewById(R.id.edt_to);

        GigItem data = mData.get(position);

        title.setText(data.getTitle());
        size.setText(data.getSize());
        from.setText("From: " + data.getFrom());
        to.setText("To: " + data.getTo());

        return view;
    }
}
