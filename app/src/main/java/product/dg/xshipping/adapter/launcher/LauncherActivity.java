package product.dg.xshipping.adapter.launcher;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import product.dg.xshipping.R;

/**
 * Created by duc.nv on 11/30/2016.
 */

public class LauncherActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        setTheme(R.style.FS_SplashScreen);
        super.onCreate(savedInstanceState, persistentState);
    }
}
