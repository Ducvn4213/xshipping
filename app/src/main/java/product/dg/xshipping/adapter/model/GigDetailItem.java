package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GigDetailItem implements Serializable {
    @SerializedName("id")
    String mID;
    @SerializedName("receiver_name")
    String mReceiverName;
    @SerializedName("phone")
    String mReceiverPhone;
    @SerializedName("address2")
    String mReceiverAddress;
    @SerializedName("city")
    String mReceiverCity;
    @SerializedName("district")
    String mReceiverDistrict;
    @SerializedName("ward")
    String mReceiverWard;

    @SerializedName("name")
    String mGigName;
    @SerializedName("weight")
    String mGigWeight;
    @SerializedName("payment_method")
    String mGigPaymentMethod;
    @SerializedName("size")
    String mGigSize;
    @SerializedName("vehicle")
    String mGigVehicle;
    @SerializedName("date")
    String mGigDate;
    @SerializedName("cod")
    String mGigCod;
    @SerializedName("note")
    String mGigNote;

    @SerializedName("address")
    String mSenderAddress;
    @SerializedName("bank_name")
    String mSenderBankName;
    @SerializedName("bank_number")
    String mSenderBankNumber;
    @SerializedName("bank_bank")
    String mSenderBankBank;

    String mSenderName;
    String mSenderPhone;

    public GigDetailItem() {}

    public String getID() {
        return mID;
    }
    public String getReceiverName() {
        return mReceiverName;
    }
    public String getReceiverPhone() {
        return mReceiverPhone;
    }
    public String getReceiverAddress() {
        return mReceiverAddress;
    }
    public String getReceiverCity() {
        return mReceiverCity;
    }
    public String getReceiverDistrict() {
        return mReceiverDistrict;
    }
    public String getReceiverWard() {
        return mReceiverWard;
    }

    public String getGigName() {
        return mGigName;
    }
    public String getGigWeight() {
        return mGigWeight;
    }
    public String getGigPaymentMethod() {
        return mGigPaymentMethod;
    }
    public String getGigSize() {
        return mGigSize;
    }
    public String getGigVehicle() {
        return mGigVehicle;
    }
    public String getGigDate() {
        return mGigDate;
    }
    public String getGigCod() {
        return mGigCod;
    }
    public String getGigNote() {
        return mGigNote;
    }

    public void setSenderName(String data) {
        mSenderName = data;
    }
    public void setSenderPhone(String data) {
        mSenderPhone = data;
    }
    public String getSenderName() {
        return mSenderName;
    }
    public String getSenderPhone() {
        return mSenderPhone;
    }
    public String getSenderAddress() {
        return mSenderAddress;
    }
    public String getSenderBankName() {
        return mSenderBankName;
    }
    public String getSenderBankNumber() {
        return mSenderBankNumber;
    }
    public String getSenderBankBank() {
        return mSenderBankBank;
    }
}
