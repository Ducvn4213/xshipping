package product.dg.xshipping;

import android.support.v7.app.AppCompatActivity;

public class MainInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void loginSuccessCallback();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doLogin(String username, String password);
        void doLoginFacebook(String id, String email, String name);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onLoginSuccess();
    }

    interface ProvidedModelOps {
        void doLogin(String username, String password);
        void doLoginFacebook(String id, String email, String name);
    }
}
